/*-----------------------------------------------------------------------------
 *  File: executor.c
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <string.h>
#include <assert.h>
#include "executor.h"
#include "port.h"
#include "misc.h"
#include "smcp1.h"

#define MCU_ACT_EXEC_EVENT_QUEUE_SIZE 50
#define MCU_EXEC_SMCP_MAX_COUNT (0x10 + 1)
#define MCU_EXECUTOR_LAZY_SUSPEND_DELAY_MS 1000

// Event queues (FIFO)
static mcu_event *event_queue_0[MCU_ACT_EXEC_EVENT_QUEUE_SIZE];
static mcu_event *event_queue_1[MCU_ACT_EXEC_EVENT_QUEUE_SIZE];
static mcu_event *event_queue_2[MCU_ACT_EXEC_EVENT_QUEUE_SIZE];
static mcu_event *event_queue_3[MCU_ACT_EXEC_EVENT_QUEUE_SIZE];
static mcu_event *event_queue_comm[MCU_ACT_EXEC_EVENT_QUEUE_SIZE];

typedef void (*attribute_handler)(const mcu_event_queue, mcu_event *, int, mcu_event_status *);

static execHandlerInfo event_cmd_exec_info[MCU_EXEC_SMCP_MAX_COUNT]; // contains a list of protocol specific command resolving functions

// Event queue markers
static uint8_t top_0;
static uint8_t bottom_0;
static uint8_t top_1;
static uint8_t bottom_1;
static uint8_t top_2;
static uint8_t bottom_2;
static uint8_t top_3;
static uint8_t bottom_3;
static uint8_t top_comm;
static uint8_t bottom_comm;

// Event queue mutexes
static osMutexId mutex_0 = PTHREAD_MUTEX_INITIALIZER;
static osMutexId mutex_1 = PTHREAD_MUTEX_INITIALIZER;
static osMutexId mutex_2 = PTHREAD_MUTEX_INITIALIZER;
static osMutexId mutex_3 = PTHREAD_MUTEX_INITIALIZER;
static osMutexId mutex_Comm = PTHREAD_MUTEX_INITIALIZER;
static osMutexId mutex_event_status = PTHREAD_MUTEX_INITIALIZER;

// Handles to tasks
static osThreadId task0;
static osThreadId task1;
static osThreadId task2;
static osThreadId task3;
static osThreadId taskComm;

// Static function prototypes
static mcu_error mcu_executorGetFIFOElements(mcu_event_queue target, uint8_t **top,
                                             uint8_t **bottom, void **rootElem);
static osMutexId *mcu_executorGetMutex(mcu_event_queue target);
static mcu_error mcu_executorInsert(mcu_event_queue target, void *fifo_addr,
                                    mcu_event **slot, uint8_t tail);
static int mcu_executorEventCount(mcu_event_queue target);
static mcu_error mcu_executorIsEmpty(mcu_event_queue target, bool *isEmpty);

static void pre_execution(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static int execute(mcu_event_queue target, mcu_event *pEvent);
static void handle_error(mcu_event_queue target, mcu_event *pEvent, int error);
static void post_execution(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus, int exec_result);
static void mcu_executorSuspend(mcu_event_queue target);
static void mcu_executorResume(mcu_event_queue target);
static void executor_attribute_handler(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus, attribute_handler handler_fptr);
static void handle_pre_execution_attributes(mcu_event_queue target, mcu_event *pEvent, int event_attr, mcu_event_status *pStatus);
static void handle_post_execution_attributes(mcu_event_queue target, mcu_event *pEvent, int event_attr, mcu_event_status *pStatus);

static int mcu_executorSizeofSbPayload(mcu_event_sb_header *pSb);
static bool mcu_executorIsBusy(mcu_event_queue target);

static mcu_error mcu_executorEventProgressUpdate(mcu_event *event, mcu_event_queue queue, bool enable, mcu_event_status *result);
static mcu_error mcu_executorEventErrorUpdate(mcu_event *event, uint32_t error, mcu_event_status *result);
static void mcu_executorFree(mcu_event *pEvent);

// Function implementations
static mcu_error mcu_executorGetFIFOElements(const mcu_event_queue target, uint8_t **top,
                                             uint8_t **bottom, void **rootElem)
{
    mcu_error retval = MCU_ERROR_NONE;
    switch (target)
    {
    case MCU_EVENT_QUEUE_0:
        *rootElem = (void *)&event_queue_0;
        *bottom = &bottom_0;
        *top = &top_0;
        break;
    case MCU_EVENT_QUEUE_1:
        *rootElem = (void *)&event_queue_1;
        *bottom = &bottom_1;
        *top = &top_1;
        break;
    case MCU_EVENT_QUEUE_2:
        *rootElem = (void *)&event_queue_2;
        *bottom = &bottom_2;
        *top = &top_2;
        break;
    case MCU_EVENT_QUEUE_3:
        *rootElem = (void *)&event_queue_3;
        *bottom = &bottom_3;
        *top = &top_3;
        break;
    case MCU_EVENT_QUEUE_COMM:
        *rootElem = (void *)&event_queue_comm;
        *bottom = &bottom_comm;
        *top = &top_comm;
        break;
    default:
        *rootElem = NULL;
        *bottom = NULL;
        *top = NULL;
        retval = MCU_ERROR_NOT_FOUND;
        break;
    }
    return retval;
}

static osMutexId *mcu_executorGetMutex(const mcu_event_queue target)
{
    osMutexId *retVal;
    switch (target)
    {
    case MCU_EVENT_QUEUE_0:
        retVal = &mutex_0;
        break;
    case MCU_EVENT_QUEUE_1:
        retVal = &mutex_1;
        break;
    case MCU_EVENT_QUEUE_2:
        retVal = &mutex_2;
        break;
    case MCU_EVENT_QUEUE_3:
        retVal = &mutex_3;
        break;
    case MCU_EVENT_QUEUE_COMM:
        retVal = &mutex_Comm;
        break;
    default:
        retVal = NULL;
        break;
    }
    return retVal;
}

mcu_error mcu_executorInit()
{
    mcu_error status = MCU_ERROR_NONE;

    memset(&event_queue_0[0], '\0', sizeof(event_queue_0));
    memset(&event_queue_1[0], '\0', sizeof(event_queue_1));
    memset(&event_queue_2[0], '\0', sizeof(event_queue_2));
    memset(&event_queue_3[0], '\0', sizeof(event_queue_3));
    memset(&event_queue_comm[0], '\0', sizeof(event_queue_comm));
    memset(&event_cmd_exec_info[0], '\0', sizeof event_cmd_exec_info);

    // Initialize protocol specific event handlers
    execHandlerInfo *pExecHandlerInfo;
    // SMCP version specific command resolvers
    // ... smcpv10
    pExecHandlerInfo = &event_cmd_exec_info[0x10];
    SMCP10_RegisterExecutor(pExecHandlerInfo);

    return status;
}

mcu_error mcu_executorInsertEvent(const mcu_event_queue target,
                                  const mcu_event *event)
{
    uint8_t *fifo_head;
    uint8_t *fifo_tail;
    void *fifo_addr;
    mcu_error status = mcu_executorGetFIFOElements(target, &fifo_head,
                                                   &fifo_tail, &fifo_addr);

    assert(event);

    if (status == MCU_ERROR_NONE)
    {
        // Store an event in bottom location
        osMutexId *mutex = mcu_executorGetMutex(target);
        osStatus semapState = osMutexWait(mutex, MCU_EXEC_MUTEX_WAIT_MS);
        if (semapState == osOK)
        {
            // Check if a queue has any room for the event.
            mcu_event **slot = (mcu_event **)(fifo_addr +
                                              (*fifo_tail * sizeof(mcu_event *)));

            if (*slot)
            {
                // Buffer full.
                // Check if we to need prioritize the new one.
                if (event->priority < (*slot)->priority)
                {
                    if (*fifo_tail > 0)
                    {
                        *fifo_tail = *fifo_tail - 1;
                    }
                    else
                    {
                        *fifo_tail = MCU_ACT_EXEC_EVENT_QUEUE_SIZE - 1;
                    }
                    slot = (mcu_event **)(fifo_addr +
                                          (*fifo_tail * sizeof(mcu_event *)));

                    status = mcu_executorEventProgressUpdate((mcu_event *)*slot, target, false, NULL);
                    mcu_executorFree((mcu_event *)*slot);
                    *slot = NULL;
                }
            }

            if (status == MCU_ERROR_NONE)
            {
                if (*slot)
                {
                    status = MCU_ERROR_OVERFLOW;
                }
                else
                {
                    // Create the actual event and put it as the very last item on FIFO queue.
                    size_t eventSize;
                    status = mcu_executorSizeOfEvent(event, &eventSize);
                    if (status == MCU_ERROR_NONE)
                    {
                        // make a copy.
                        *slot = mcuMalloc(eventSize);

                        if (*slot)
                        {
                            memcpy((char *)*slot, event, eventSize);
                            status = mcu_executorEventProgressUpdate((mcu_event *)*slot, target, true, NULL);
                            *fifo_tail = *fifo_tail + 1;
                            if (*fifo_tail >= MCU_ACT_EXEC_EVENT_QUEUE_SIZE)
                                *fifo_tail = 0;
                        }
                        else
                        {
                            status = MCU_ERROR_NO_MEMORY;
                        }
                    }

                    // Final step. Move the event in right priority location.
                    if (status == MCU_ERROR_NONE)
                    {
                        status = mcu_executorInsert(target, fifo_addr, slot, *fifo_tail);
                    }
                }
                semapState = osMutexRelease(mutex);
                // Notify the target executor
                mcu_executorResume(target);
            }
        }
        else
        {
            status = MCU_ERROR_TIMEOUT;
        }
        assert(semapState == osOK);
    }
    return status;
}

mcu_error mcu_executorPopEvent(const mcu_event_queue target, mcu_event **pEvent)
{
    uint8_t *fifo_head;
    uint8_t *fifo_tail;
    void *fifo_addr;

    mcu_error status = mcu_executorGetFIFOElements(target, &fifo_head,
                                                   &fifo_tail, &fifo_addr);

    if (status == MCU_ERROR_NONE)
    {

        status = MCU_ERROR_TIMEOUT;
        osMutexId *mutex = mcu_executorGetMutex(target);
        osStatus semapState = osMutexWait(mutex, MCU_EXEC_MUTEX_WAIT_MS);

        if (semapState == osOK)
        {
            // Get the top most event
            mcu_event **slot = (mcu_event **)(fifo_addr +
                                              (*fifo_head * sizeof(mcu_event *)));

            if (*slot == NULL)
                status = MCU_ERROR_UNDERFLOW;
            else
            {
                // Set return value first...
                *pEvent = *slot;
                *slot = NULL;
                // ... and update the top
                *fifo_head = *fifo_head + 1;
                if (*fifo_head >= MCU_ACT_EXEC_EVENT_QUEUE_SIZE)
                    *fifo_head = 0;
                status = MCU_ERROR_NONE;
            }
            semapState = osMutexRelease(mutex);
        }
        assert(semapState == osOK);
    }
    return status;
}

static int mcu_executorEventCount(const mcu_event_queue target)
{
    int retval = -1;
    uint8_t *fifo_head;
    uint8_t *fifo_tail;
    void *fifo_addr; // address on queue
    mcu_error status = mcu_executorGetFIFOElements(target, &fifo_head,
                                                   &fifo_tail, &fifo_addr);

    if (status == MCU_ERROR_NONE)
    {
        if (*fifo_head == *fifo_tail)
        {
            mcu_event **top_event = (mcu_event **)(fifo_addr +
                                                   (*fifo_head * sizeof(mcu_event *)));

            if (*top_event == NULL)
                retval = 0;
            else
                retval = MCU_ACT_EXEC_EVENT_QUEUE_SIZE;
        }
        else if (*fifo_tail >= *fifo_head)
            retval = *fifo_tail - *fifo_head;
        else
            retval = (MCU_ACT_EXEC_EVENT_QUEUE_SIZE - *fifo_head) + *fifo_tail;
    }
    return retval;
}

_Noreturn void mcu_executorStart(const mcu_event_queue target)
{
    mcu_error err;
    mcu_event *pEvent;

    osThreadId taskid = osThreadGetId();
    switch (target)
    {
    case MCU_EVENT_QUEUE_0:
        task0 = taskid;
        break;
    case MCU_EVENT_QUEUE_1:
        task1 = taskid;
        break;
    case MCU_EVENT_QUEUE_2:
        task2 = taskid;
        break;
    case MCU_EVENT_QUEUE_3:
        task3 = taskid;
        break;
    case MCU_EVENT_QUEUE_COMM:
        taskComm = taskid;
        break;
    default:
        assert(false); // This cannot happen. Assert always.
        break;
    }

    mcu_event_status statusSnapshot;

    for (;;)
    {

        if (SHOULD_EXIT && PROXY_INIT_COMPLETE)
        {
            printf("Exiting executor loop due to should_exit flag.\n");
            pthread_exit(NULL);
            // osThreadTerminate(taskid);
        }

        // Pop takes an ownership of heap allocated for 'pEvent'
        err = mcu_executorPopEvent(target, &pEvent);
        if (!err)
        {
            int result;
            mcu_event_queue execTarget = target;
            // Pre-execution activities
            pre_execution(execTarget, pEvent, NULL);
            // Execute the event
            result = execute(execTarget, pEvent);
            // Executed. Update the status progress accordingly.
            mcu_executorEventProgressUpdate(pEvent, execTarget, false, &statusSnapshot);
            // Handle error if needed
            if (result)
            {
                mcu_executorEventErrorUpdate(pEvent, (uint32_t)result, &statusSnapshot);
                handle_error(execTarget, pEvent, result);
            }
            // Post execution activities
            post_execution(execTarget, pEvent, &statusSnapshot, result);

            // Release the allocated resources
            mcu_executorFree(pEvent);
        }
        else if (err == MCU_ERROR_UNDERFLOW)
        {
            // Empty task queue.
            // Just yield if we are busy - suspend otherwise
            if (!mcu_executorIsBusy(target))
                mcu_executorSuspend(target);
            else
                osThreadYield();
        }
    }
}

static void pre_execution(const mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    // Handle event attributes
    attribute_handler pre_exec_attr_handler = &handle_pre_execution_attributes;
    executor_attribute_handler(target, pEvent, pStatus, pre_exec_attr_handler);
}

static int execute(const mcu_event_queue target, mcu_event *pEvent)
{
    int retval = MCU_ERROR_NOT_FOUND;

    // Get a protocol specific command resolver
    cmd_resolver doResolve = event_cmd_exec_info[pEvent->frame.version].cmdResolver;

    // ... resolve a command handler
    if (doResolve)
    {
        cmd_exec_handler doExecute = doResolve(pEvent->frame.type);

        // ... and execute the command.
        if (doExecute)
            retval = doExecute(target, pEvent);
    }
    return retval;
}

static void handle_error(const mcu_event_queue target, mcu_event *pEvent, int error)
{
    UNUSED(target);
    error_handler doHandleError = event_cmd_exec_info[pEvent->frame.version].execErrorHandler;
    if (doHandleError)
        doHandleError(pEvent, error);
}

static void post_execution(const mcu_event_queue target, mcu_event *pEvent,
                           mcu_event_status *pStatus, int exec_result)
{
    // Handle event attributes
    attribute_handler post_exec_attr_handler = &handle_post_execution_attributes;
    executor_attribute_handler(target, pEvent, pStatus, post_exec_attr_handler);

    // TBD. Do whatever is needed
    // Print a debug message
    if (exec_result)
    {
        MCUTRACE(("Error at actuator ('%d') event ('%d') execution. Error code: (%d) \r\n",
                  target, pEvent->frame.type, exec_result));
    }
}

static void mcu_executorSuspend(const mcu_event_queue target)
{
    bool suspend = true;

    if (target != MCU_EVENT_QUEUE_COMM)
    {

        uint32_t tickstart = 0;
        tickstart = HAL_GetTick();
        bool isEmpty;

        while (!mcu_commonIsExpired(HAL_GetTick(), tickstart,
                                    MCU_EXECUTOR_LAZY_SUSPEND_DELAY_MS))
        {
            if (mcu_executorIsEmpty(target, &isEmpty) == MCU_ERROR_NONE)
            {
                if (!isEmpty)
                {
                    suspend = false;
                    break;
                }
            }
            osThreadYield();
        }

        if (suspend)
        {
            // Something to be done here?
        }
    }

    if (suspend)
        osThreadSuspend(0);
}

static void mcu_executorResume(const mcu_event_queue target)
{
    osThreadId task_hdl;

    switch (target)
    {
    case MCU_EVENT_QUEUE_0:
        task_hdl = task0;
        break;
    case MCU_EVENT_QUEUE_1:
        task_hdl = task1;
        break;
    case MCU_EVENT_QUEUE_2:
        task_hdl = task2;
        break;
    case MCU_EVENT_QUEUE_3:
        task_hdl = task3;
        break;
    case MCU_EVENT_QUEUE_COMM:
        task_hdl = taskComm;
        break;
    default:
        task_hdl = (osThreadId)NULL;
        break;
    }

    // Resume the target task
    if (task_hdl)
    {
        osThreadResume(task_hdl);
    }
}

static bool mcu_executorIsBusy(const mcu_event_queue target)
{
    // Actuators are busy when PWM is active.
    bool retval;
    if (target == MCU_EVENT_QUEUE_COMM)
        retval = false;
    else
    {
        retval = mcu_pwmIsBusy((mcu_actuator)target, NULL);
    }
    return retval;
}

static void executor_attribute_handler(const mcu_event_queue target,
                                       mcu_event *pEvent, mcu_event_status *pStatus, attribute_handler handler_fptr)
{
    if (pEvent->frame.options)
    {

        int index;

        for (index = MCU_EVENT_OPTIONS_MAX_COUNT - 1; index > 0; index--)
        {
            if (pEvent->frame.options & (1 << index))
                handler_fptr(target, pEvent, index + 1, pStatus);
        }
    }
}

static void handle_pre_execution_attributes(const mcu_event_queue target,
                                            mcu_event *pEvent, int event_attr, mcu_event_status *pStatus)
{
    // Get a function pointer to attribute specific handler function
    pre_exec_handler *do_handle_fptr =
        event_cmd_exec_info[pEvent->frame.version].preCmdHandlers;
    do_handle_fptr = do_handle_fptr + (event_attr - 1);

    // Call the handler
    if (*do_handle_fptr)
        (*do_handle_fptr)(target, pEvent, pStatus);
}

static void handle_post_execution_attributes(const mcu_event_queue target,
                                             mcu_event *pEvent, int event_attr, mcu_event_status *pStatus)
{
    // Get a function pointer to attribute specific handler function
    post_exec_handler *do_handle_fptr =
        event_cmd_exec_info[pEvent->frame.version].postCmdHandlers;
    do_handle_fptr = do_handle_fptr + (event_attr - 1);

    // Call the handler
    if (*do_handle_fptr)
        (*do_handle_fptr)(target, pEvent, pStatus);
}

mcu_error mcu_executorSizeOfEvent(const mcu_event *event, size_t *size)
{
    assert(event);
    mcu_error retval = MCU_ERROR_NONE;
    int sb_index;
    int sb_size;
    int totalSize = sizeof(mcu_event);
    mcu_event_sb_header *pSb = (void *)(void *)event + sizeof(mcu_event);

    for (sb_index = 0; sb_index < event->frame.sub_blocks; sb_index++)
    {
        sb_size = sizeof(smcp1_sb_header);
        sb_size += mcu_executorSizeofSbPayload(pSb);
        totalSize += sb_size;
        pSb = (void *)(void *)pSb + sb_size;
    }

    *size = totalSize;

    return retval;
}

static mcu_error mcu_executorInsert(const mcu_event_queue target,
                                    void *fifo_addr, mcu_event **slot, uint8_t tail)
{
    mcu_error retval = MCU_ERROR_NONE;
    int i;
    int tmp_tail = (tail & 0xFF);
    int event_cnt = mcu_executorEventCount(target);

    // Do not count the current event
    event_cnt--;

    // tmp_tail points to next free location
    // ... reduce the value by one
    if (tmp_tail > 0)
        tmp_tail--;
    else
        tmp_tail = MCU_ACT_EXEC_EVENT_QUEUE_SIZE - 1;

    for (i = 0; i < event_cnt; i++)
    {
        // Move tmp_tail to point previous event in queue.
        tmp_tail--;
        if (tmp_tail < 0)
            tmp_tail = MCU_ACT_EXEC_EVENT_QUEUE_SIZE - 1;

        // Get a point to the previous event
        mcu_event **prevEvent = (mcu_event **)(fifo_addr +
                                               (tmp_tail * sizeof(mcu_event *)));

        // This just ensures that we won't do a self compare by mistake.
        assert(*prevEvent != *slot);

        if (!*prevEvent)
            break;
        if ((*prevEvent)->priority <= (*slot)->priority)
            break;
        else
        {
            // The new event has a higher priority than previous
            // We need to swap the location of these two events in queue.
            mcu_event *tmp_event = *prevEvent;
            *prevEvent = *slot;
            *slot = tmp_event;
            slot = prevEvent;
        }
    }
    return retval;
}

static int mcu_executorSizeofSbPayload(mcu_event_sb_header *pSb)
{
    int payloadSize;
    int typeSize;

    switch (pSb->type)
    {

    case EVENT_SB_TYPE_VOID:
        typeSize = sizeof(void);
        break;
    case EVENT_SB_TYPE_UINT8:
        typeSize = sizeof(uint8_t);
        break;
    case EVENT_SB_TYPE_INT8:
        typeSize = sizeof(int8_t);
        break;
    case EVENT_SB_TYPE_UINT16:
        typeSize = sizeof(uint16_t);
        break;
    case EVENT_SB_TYPE_INT16:
        typeSize = sizeof(int16_t);
        break;
    case EVENT_SB_TYPE_UINT32:
        typeSize = sizeof(uint32_t);
        break;
    case EVENT_SB_TYPE_INT32:
        typeSize = sizeof(int32_t);
        break;
    case EVENT_SB_TYPE_UINT64:
        typeSize = sizeof(uint64_t);
        break;
    case EVENT_SB_TYPE_INT64:
        typeSize = sizeof(int64_t);
        break;
    case EVENT_SB_TYPE_UINT8_ARRAY_2:
        typeSize = 2 * sizeof(uint8_t);
        break;
    case EVENT_SB_TYPE_INT8_ARRAY_2:
        typeSize = 2 * sizeof(int8_t);
        break;
    case EVENT_SB_TYPE_UINT8_ARRAY_3:
        typeSize = 3 * sizeof(uint8_t);
        break;
    case EVENT_SB_TYPE_INT8_ARRAY_3:
        typeSize = 3 * sizeof(int8_t);
        break;
    case EVENT_SB_TYPE_UINT8_ARRAY_4:
        typeSize = 4 * sizeof(uint8_t);
        break;
    case EVENT_SB_TYPE_INT8_ARRAY_4:
        typeSize = 4 * sizeof(int8_t);
        break;
    case EVENT_SB_TYPE_UINT16_ARRAY_2:
        typeSize = 2 * sizeof(uint16_t);
        break;
    case EVENT_SB_TYPE_INT16_ARRAY_2:
        typeSize = 2 * sizeof(int16_t);
        break;
    default:
        typeSize = 0;
        break;
    }

    // The actual data size in bytes
    payloadSize = pSb->size * typeSize;
    // ... but since data must be 32 bit aligned
    // ... we calculate the size of end paddings also.
    payloadSize += payloadSize % 4;

    return payloadSize;
}

static mcu_error mcu_executorIsEmpty(const mcu_event_queue target, bool *isEmpty)
{
    uint8_t *fifo_head;
    uint8_t *fifo_tail;
    void *fifo_addr;

    mcu_error retval = mcu_executorGetFIFOElements(target, &fifo_head,
                                                   &fifo_tail, &fifo_addr);

    if (retval == MCU_ERROR_NONE)
    {
        osMutexId *mutex = mcu_executorGetMutex(target);
        osStatus semapState = osMutexWait(mutex, 1000);

        if (semapState == osOK)
        {

            // Get the top most event
            mcu_event **slot = (mcu_event **)(fifo_addr +
                                              (*fifo_head * sizeof(mcu_event *)));

            if (*slot == NULL)
                *isEmpty = true;
            else
                *isEmpty = false;

            semapState = osMutexRelease(mutex);
        }
        if (semapState != osOK)
            retval = MCU_ERROR_TIMEOUT;
    }

    return retval;
}

bool mcu_executorIsEventDone(mcu_event_status *pStatus)
{
    bool retval = true;
    for (int i = MCU_EXECUTOR_EVENT_LOCKS_CNT - 1; i >= 0; i--)
    {
        if (pStatus->locks[i])
        {
            retval = false;
            break;
        }
    }
    return retval;
}

static mcu_error mcu_executorEventProgressUpdate(mcu_event *event, const mcu_event_queue queue,
                                                 bool enable, mcu_event_status *result)
{
    mcu_error retval = MCU_ERROR_NONE;

    osStatus mutexState = osMutexWait(&mutex_event_status, MCU_EXEC_MUTEX_WAIT_MS);
    if (mutexState == osOK)
    {
        // Allocate the data buf when needed.

        if (event->pStatus == NULL)
        {
            event->pStatus = (mcu_event_status *)mcuMalloc(sizeof(mcu_event_status));
            if (event->pStatus)
            {
                memset((void *)event->pStatus, 0, sizeof(mcu_event_status));
            }
            else
            {
                retval = MCU_ERROR_NO_MEMORY;
            }
        }

        // Copy the data content
        if (retval == MCU_ERROR_NONE)
        {
            uint32_t lockIndex;
            if (queue <= MCU_EVENT_QUEUE_3)
            {
                lockIndex = queue;
            }
            else
            {
                lockIndex = MCU_EVENT_QUEUE_3 + 1; // MCU_EVENT_QUEUE_COMM
            }

            volatile uint32_t *pLockCnt = &event->pStatus->locks[lockIndex];
            if (enable)
            {
                (*pLockCnt)++;
            }
            else
            {
                (*pLockCnt)--;
            }
            // Save the snapshot if requested
            if (result)
            {
                *result = *event->pStatus;
            }

            // Release the pStatus when the event is done
            if (mcu_executorIsEventDone(event->pStatus))
            {
                mcuFree(event->pStatus);
                event->pStatus = NULL;
            }
        }

        mutexState = osMutexRelease(&mutex_event_status);
    }

    if (mutexState != osOK && retval == MCU_ERROR_NONE)
    {
        retval = MCU_ERROR_TIMEOUT;
    }

    return retval;
}

static mcu_error mcu_executorEventErrorUpdate(mcu_event *event, const uint32_t error,
                                              mcu_event_status *result)
{
    mcu_error retval = MCU_ERROR_NONE;

    osStatus mutexState = osMutexWait(&mutex_event_status, MCU_EXEC_MUTEX_WAIT_MS);
    if (mutexState == osOK)
    {

        if (event->pStatus)
        {
            event->pStatus->error = error;

            // Save the snapshot if requested
            if (result)
                *result = *event->pStatus;
        }
        else
            retval = MCU_ERROR_NOT_INITIALIZED;

        mutexState = osMutexRelease(&mutex_event_status);
    }

    if (mutexState != osOK && retval == MCU_ERROR_NONE)
        retval = MCU_ERROR_TIMEOUT;

    return retval;
}

mcu_error mcu_executorStatusInsertPayload(mcu_event_status *pStatus, char *pPayload)
{
    mcu_error retval = MCU_ERROR_NONE;
    osStatus mutexState = osMutexWait(&mutex_event_status, MCU_EXEC_MUTEX_WAIT_MS);
    if (mutexState == osOK)
    {
        if (pStatus)
        {
            assert((pStatus->pDataDeferred && pPayload == NULL) || !pStatus->pDataDeferred);
            pStatus->pDataDeferred = pPayload;
        }
        else
        {
            retval = MCU_ERROR_NOT_INITIALIZED;
        }
        mutexState = osMutexRelease(&mutex_event_status);
    }

    if (mutexState != osOK && retval == MCU_ERROR_NONE)
    {
        retval = MCU_ERROR_TIMEOUT;
    }

    return retval;
}

mcu_error mcu_executorDelegateEvent(mcu_event *target, mcu_event *source)
{
    mcu_error retval = MCU_ERROR_NONE;

    // Copy meaningful fields from the source

    // SMCPv1 fields
    target->frame.version = source->frame.version;
    target->frame.sender_type = source->frame.sender_type;
    target->frame.receiver_id = source->frame.receiver_id;
    target->frame.sender_id = source->frame.sender_id;
    target->frame.message_id = source->frame.message_id;
    target->frame.options = source->frame.options;
    target->rootType = source->rootType;
    target->extra2 = source->extra2;

    // mcu_event specific fields
    target->priority = source->priority;
    memcpy((void *)&target->replyaddr, (void *)&source->replyaddr, sizeof(struct sockaddr_in));
    target->pData1 = NULL;
    target->pStatus = NULL;

    // Clean the options of source event. The delegate event is now responsible for resp, notification etc.
    source->frame.options = 0;

    return retval;
}

mcu_error mcu_executorRemoveAll(const mcu_event_queue target)
{
    // Pop events and release resources until the queue is clear.
    mcu_error retval;
    mcu_event *pEvent;
    mcu_event_status statusSnapshot;

    do
    {
        // Pop the next event
        retval = mcu_executorPopEvent(target, &pEvent);
        if (retval == MCU_ERROR_NONE)
        {
            // Update the progress ...
            retval = mcu_executorEventProgressUpdate(pEvent, target, false,
                                                     &statusSnapshot);
            // ... release deferred data if possible
            if (retval == MCU_ERROR_NONE && !pEvent->pStatus && statusSnapshot.pDataDeferred)
            {
                mcuFree((char *)statusSnapshot.pDataDeferred);
                mcu_executorStatusInsertPayload(&statusSnapshot, NULL);
            }
            // ... and release the event resources finally.
            mcu_executorFree(pEvent);
        }

    } while (retval == MCU_ERROR_NONE);

    // Expected result is underflow - every time.
    if (retval == MCU_ERROR_UNDERFLOW)
        retval = MCU_ERROR_NONE;

    return retval;
}

static void mcu_executorFree(mcu_event *pEvent)
{
    if (pEvent->pData1)
    {
        mcuFree(pEvent->pData1);
    }
    mcuFree(pEvent);
}
