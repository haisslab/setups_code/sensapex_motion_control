/*-----------------------------------------------------------------------------
 *  File: tmcl.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef TMCL_H
#define TMCL_H

#include "constants.h"
#include <stdint.h>
#include <stdbool.h>

#define TMCL_MSG_SIZE   9
#define TMCL_MAX_AXIS_COUNT     4
#define TMCL_AXIS_PARAM_MAX_POSITIONING_SPEED   2047
#define TMCL_TO_SMCP_COORDINATE_MULTIPLIER      1000
#define SMCP_TO_TMCL_STEP_MULTIPLIER            100
#define SMCP_TSC_MAX_POSITIONING_SPEED          7776
#define SMCP_TO_TMCL_SPEED_MULTIPLIER           ((float)TMCL_AXIS_PARAM_MAX_POSITIONING_SPEED / SMCP_TSC_MAX_POSITIONING_SPEED)

typedef struct {
    uint8_t moduleAddr;    // Module address
    uint8_t cmd;            // Command number
    uint8_t type;           // Type number
    uint8_t addr;           // Motor or Bank number
    uint32_t data;          // Value (MSB first!)
    uint8_t checkSum;       // Checksum
} tmclReq;

typedef struct {
    uint8_t replyAddr;
    uint8_t moduleAddr;
    uint8_t status;         // Status (e.g. 100 means “no error”)
    uint8_t cmd;            // Command Number
    uint32_t data;          // Value (MSB first!)
    uint8_t checkSum;       // Checksum
} tmclResp;

typedef enum {
    tmclStatusWrongChecksum         = 1,
    tmclStatusInvalidCommand        = 2,
    tmclStatusWrongType             = 3,
    tmclStatusInvalidValue          = 4,
    tmclStatusConfigEepromLocked    = 5,
    tmclStatusCommandNotAvailable   = 6,
    tmclStatusOk                    = 100,
    tmclStatusCommandLoaded         = 101
} tmclStatus;

typedef enum {
    tmclCmdRotateRight              = 1,
    tmclCmdRotateLeft               = 2,
    tmclCmdStop                     = 3,
    tmclCmdMoveToPosition           = 4,
    tmclCmdSetAxisParameter         = 5,
    tmclCmdGetAxisParameter         = 6,
    tmclCmdStoreAxisParameter       = 7,
    tmclCmdRestoreAxisParameter     = 8,
    tmclCmdSetGlobalParameter       = 9,
    tmclCmdGetGlobalParameter       = 10,
    tmclCmdStoreGlobalParameter     = 11,
    tmclCmdRestoreGlobalParameter   = 12,
    tmclGetCoordinate               = 31,
    tmclCustomGetFirmwareVersion    = 136,
    tmclCustomLocal                 = 255
} tmclCmd;

typedef enum {
    tmclAxisParamTargetPosition             = 0,
    tmclAxisParamActualPosition             = 1,
    tmclAxisParamTargetSpeed                = 2,
    tmclAxisParamActualSpeed                = 3,
    tmclAxisParamMaxPositioningSpeed        = 4,
    tmclAxisParamMaxPositioningAcceleration = 5,
    tmclAxisParamMaxAbsCurrent              = 6,
    tmclAxisParamStandByCurrent             = 7,
    tmclAxisParamTargetPosReached           = 8,
    tmclAxisParamRefSwitchStatus            = 9,
    tmclAxisParamRightLimitSwitchStatus     = 10,
    tmclAxisParamLeftLimitSwitchStatus      = 11,
    tmclAxisParamRightLimitSwitchDisable    = 12,
    tmclAxisParamLeftLimitSwitchDisable     = 13
} tmclAxisParam;

typedef enum {
    tmclGlobalParamSerialAddress            = 66
} tmclGlobalParam;

bool tmclValidateResp(const tmclResp *aResp);
void tmclHeartBeatHandler();

void tmclSerializeReq(tmclReq *aTmclReq, uint8_t *aBuffer);
void tmclDeserializeResp(uint8_t *aBuffer, tmclResp *aTmclResp);

uint8_t tmclCalculateCheckSumReq(const tmclReq *aReq);
uint8_t tmclCalculateCheckSumResp(const tmclResp *aResp);

int32_t tmclGetPosition(uint8_t aAxis, bool rawData);
int32_t tmclSetPosition(uint8_t aAxis, int32_t aPosition);

#endif
