/*-----------------------------------------------------------------------------
 *  File: misc.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef MISC_H
#define MISC_H

#include <signal.h>
#include "constants.h"
#include "executor.h"
#include <stdbool.h>

// Autopilot speeds (um/s)
#define SMCP_AUTOPILOT_SPEED_1 125
#define SMCP_AUTOPILOT_SPEED_2 500
#define SMCP_AUTOPILOT_SPEED_3 1000
#define SMCP_AUTOPILOT_SPEED_4 1500
#define SMCP_AUTOPILOT_SPEED_5 2000

#define MCU_COMMON_FEATURES_MASK (UINT64_MAX)

// External declaration of the SHOULD_EXIT variable
extern volatile sig_atomic_t SHOULD_EXIT;

extern volatile sig_atomic_t PROXY_INIT_COMPLETE;

typedef struct
{
    uint32_t serialNbr;
    uint32_t manufactured;
    uint32_t hwid;
} hwInfo;

typedef struct
{
    uint64_t features;
    uint64_t rd_features;
    uint16_t axis;
    uint16_t smcp_version;
    uint16_t udp_port;
    uint64_t features_mask;
} systemInfo;

typedef enum
{
    pwmDirection_Forward = 0,
    pwmDirection_Backward = 1,
    pwmDirection_Unknown = 9
} pwmDirection;

mcu_error initSystem();
const hwInfo *mcu_commonGetHwInfo(bool refreshCache);
systemInfo *mcu_commonGetSystemConf(bool refreshCache);
mcu_error mcu_commonSetSystemConf(systemInfo *info, bool writeThrough);
bool mcu_pwmIsBusy(mcu_actuator actuator, int *state);
bool mcu_commonIsExpired(uint32_t current_ms, uint32_t start_ms, uint32_t period_ms);
bool mcu_commonIsActuatorEnabled(mcu_actuator act, const systemInfo *info);
mcu_error mcu_actuatorGetPosition(mcu_actuator act, int *position, bool rawData);
mcu_error mcu_actuatorSetBusyStatus(mcu_actuator actuator, uint32_t status);

#endif
