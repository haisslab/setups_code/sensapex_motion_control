#!/bin/bash

# Get the directory of the current script
SCRIPT_DIR=$(dirname "$(realpath "$0")")

# Path to the PID file
PIDFILE="/var/run/sensapex_proxy.pid"

# Path to the log file
LOGFILE="/var/log/sensapex_proxy.log"

SLEEP_TIME=5

# Path to the task executable or script
TASK="$SCRIPT_DIR/executeable/smcp1_proxy"

# Function to start the task
start_task() {
    echo "Starting task..." >> "$LOGFILE"
    $TASK &
    echo $! > "$PIDFILE"
}

# Monitor the task and restart if it crashes
while true; do
    PID=$(cat "$PIDFILE")
    if ! ps -p $PID > /dev/null; then
        echo "Task crashed. Restarting..." >> "$LOGFILE"
        start_task
    fi
    echo "Restarting task in $SLEEP_TIME seconds..." >> "$LOGFILE"
    sleep $SLEEP_TIME
done