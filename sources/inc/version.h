/*-----------------------------------------------------------------------------
 *  File: version.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2023-2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef VERSION_H
#define VERSION_H

#define SMCP_PROXY_VERSION_MAJOR 0
#define SMCP_PROXY_VERSION_YEAR 24
#define SMCP_PROXY_VERSION_WEEK 23
#define SMCP_PROXY_VERSION_DAY_AND_BUILD 100

#endif
