/*-----------------------------------------------------------------------------
 *  File: constants.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <stdint.h>

#define MCU_32_BIT_DATA_UNDEFINED 0xFFFFFFFF
#define MCU_16_BIT_DATA_UNDEFINED 0xFFFF
#define MCU_8_BIT_DATA_UNDEFINED 0xFF

// TODO. Now here - later ?
#define UNUSED(X) (void)X /* To avoid gcc/g++ warnings */

typedef enum
{
    SpErrorOk = 0,
    SpErrorRead = 1,
    SpErrorWrite = 2,
    SpErrorInvalid = 3,
    SpErrorNoMemory = 4,
    SpErrorParameter = 5,
    SpErrorUnknown = 99
} SpError;

typedef enum
{
    MCU_ERROR_NONE = 0,
    MCU_ERROR_NOT_SUPPORTED = 1,
    MCU_ERROR_NOT_FOUND = 2,
    MCU_ERROR_TIMEOUT = 3,
    MCU_ERROR_OVERFLOW = 4,
    MCU_ERROR_UNDERFLOW = 5,
    MCU_ERROR_NO_MEMORY = 6,
    MCU_ERROR_BUSY = 7,
    MCU_ERROR_NOT_INITIALIZED = 8,
    MCU_ERROR_PARAMETER = 9,
    MCU_ERROR_NO_ACCESS = 10,
    MCU_ERROR_CANCEL = 11,
    MCU_ERROR_NOT_ENABLED = 12,
    MCU_ERROR_NOT_ALLOWED = 13,
    MCU_ERROR_UDP_PEER_LOST = 14,
    MCU_ERROR_GENERAL = 99
} mcu_error;

/*
typedef enum
{
    ACTUATOR_0  = 0,
    ACTUATOR_1  = 1,
    ACTUATOR_2  = 2,
    ACTUATOR_3  = 3
} mcu_actuator;
*/

typedef uint32_t mcu_actuator;

#define ACTUATOR_0 0 // by default : axis X
#define ACTUATOR_1 1 // by default : axis Y
#define ACTUATOR_2 2 // by default : axis Z
#define ACTUATOR_3 5 // by default : axis A

#define MCU_SYSTEM_ACT_MAX_COUNT 4 // total : 4 actuators

extern const mcu_actuator actuators[MCU_SYSTEM_ACT_MAX_COUNT];

#define ACTUATOR_FIRST 0
#define ACTUATOR_LAST (MCU_SYSTEM_ACT_MAX_COUNT - 1)

// #define ACTUATOR_FIRST ACTUATOR_0
// #define ACTUATOR_LAST ACTUATOR_2

#endif
