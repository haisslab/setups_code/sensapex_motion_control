/*-----------------------------------------------------------------------------
 *  File: functions.c
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2023-2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "functions.h"
#include "constants.h"
#include "port.h"
#include "smcp1.h"
#include "tmcl.h"
#include "misc.h"

#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <ifaddrs.h>
#include <pthread.h>
#include <errno.h>

#define MAX_BUFFER_SIZE 1024

#define MAX_SERIAL_PORTS 10
const char *serial_ports[MAX_SERIAL_PORTS] = {
    "/dev/ttyACM0",
    "/dev/ttyACM1",
    "/dev/ttyACM2",
    "/dev/ttyACM3",
    "/dev/ttyACM4",
    "/dev/ttyACM5",
    "/dev/ttyACM6",
    "/dev/ttyACM7",
    "/dev/ttyACM8",
    "/dev/ttyACM9"};

static pthread_mutex_t serialMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t udpMutex = PTHREAD_MUTEX_INITIALIZER;

static int udp_sock = 0;
int serial_fd = 0;
static struct sockaddr_in r_addr;

static mcu_error processSmcp1Request(char *buffer, struct sockaddr *recv_addr, int bytes_read);
static int isOwnIp(struct sockaddr *addr);

static int openSerialPort(const char *serial_port)
{
    int fd = open(serial_port, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        perror("Error opening serial port");
        return -1;
        // if it cannot find the port, instead of closing the program, we return -1 to search other ports
        // exit(EXIT_FAILURE);
    }

    struct termios tty;
    memset(&tty, 0, sizeof tty);
    if (tcgetattr(fd, &tty) != 0)
    {
        perror("Error from tcgetattr");
        exit(EXIT_FAILURE);
    }

    cfsetospeed(&tty, B115200);
    cfsetispeed(&tty, B115200);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8; // 8-bit chars
    tty.c_iflag &= ~IGNBRK;                     // disable break processing
    tty.c_lflag = 0;                            // no signaling chars, no echo, no canonical processing
    tty.c_oflag = 0;                            // no remapping, no delays
    tty.c_cc[VMIN] = 0;                         // read doesn't block
    tty.c_cc[VTIME] = 5;                        // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl
    tty.c_cflag |= (CLOCAL | CREAD);        // ignore modem controls, enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= 0;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr(fd, TCSANOW, &tty) != 0)
    {
        perror("Error from tcsetattr");
        exit(EXIT_FAILURE);
    }

    speed_t input_baud = cfgetispeed(&tty);
    speed_t output_baud = cfgetospeed(&tty);
    printf("Input Serial Port Baud Rate: %d\n", input_baud);
    printf("Output Serial Port Baud Rate: %d\n", output_baud);

    printf("serial_fd value: %d\n", fd);
    return fd;
}

void proxyInit(unsigned int udp_port, char *serial_device, const char *udp_address)
{
    struct sockaddr_in l_addr;

    // Init communication interfaces

    // ... SERIAL
    // trying with the given port
    serial_fd = openSerialPort((const char *)serial_device);

    // if we didn't foundit, try with default ports list
    if (serial_fd == -1)
    {
        printf("Failed to open the provided serial port: %s\n", serial_device);
        printf("Falling back to scanning serial ports from the default list :\n");
        for (int i = 0; i < MAX_SERIAL_PORTS; i++)
        {
            serial_fd = openSerialPort(serial_ports[i]);
            if (serial_fd < 0)
            {
                printf("Failed to open serial port: %s\n", serial_ports[i]);
            }
            else
            {
                printf("Successfully opened serial port: %s\n", serial_ports[i]);
                break;
            }
        }
    }

    if (serial_fd < 0)
    {
        fprintf(stderr, "Failed to open any serial port. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    // ... UDP
    if ((udp_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&l_addr, 0, sizeof(l_addr));
    memset(&r_addr, 0, sizeof(r_addr));

    // Use IPv4 interface
    l_addr.sin_family = AF_INET; // IPv4
    l_addr.sin_addr.s_addr = INADDR_ANY;
    l_addr.sin_port = htons(udp_port);

    in_addr_t target_addr = inet_addr(udp_address);
    r_addr.sin_family = AF_INET; // IPv4
    r_addr.sin_addr.s_addr = target_addr;
    r_addr.sin_port = htons(udp_port);

    // Reuse own IP address
    int reuse_addr = 1;
    if (setsockopt(udp_sock, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr)) < 0)
    {
        perror("setsockopt (reuse_addr) failed");
        exit(EXIT_FAILURE);
    }

    // Permit sending broadcast messages
    int allow_broadcast = 1;
    if (setsockopt(udp_sock, SOL_SOCKET, SO_BROADCAST, &allow_broadcast, sizeof(allow_broadcast)) < 0)
    {
        perror("setsockopt (broadcast) failed");
        exit(EXIT_FAILURE);
    }

    // Bind socket to server address
    if (bind(udp_sock, (const struct sockaddr *)&l_addr, sizeof(l_addr)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
}

void proxyDeinit()
{
    if (serial_fd)
    {
        close(serial_fd);
        serial_fd = 0;
    }
    if (udp_sock)
    {
        close(udp_sock);
        udp_sock = 0;
    }
}

void *readUdp(void *arg)
{
    UNUSED(arg);

    char buffer[MAX_BUFFER_SIZE];
    socklen_t len = sizeof(r_addr);
    struct sockaddr recv_addr;

    // Set a timeout for the recvfrom function
    // struct timeval timeout;
    // timeout.tv_sec = 2; // 5 seconds timeout
    // timeout.tv_usec = 0;

    // if (setsockopt(udp_sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0)
    // {
    //     perror("setsockopt failed");
    //     SHOULD_EXIT = 1;
    //     return NULL;
    // }

    for (;;)
    {
        int bytes_read = (int)recvfrom(udp_sock, buffer, MAX_BUFFER_SIZE, 0, &recv_addr, &len);
        if (bytes_read > 0 && !isOwnIp(&recv_addr))
        {
            // printf("Controller message recieved %d\n", bytes_read);
            processSmcp1Request(buffer, &recv_addr, bytes_read);
        }
        else if (bytes_read < 0)
        {
            // if (errno == EWOULDBLOCK || errno == EAGAIN)
            // {
            //     // printf("recvfrom timeout, no data received.\n");
            // }
            // else
            // {
            // perror("recvfrom failed");
            if (PROXY_INIT_COMPLETE)
            {
                printf("ERROR : UDP-SOCKET COMMUNICATION FAILED, CLOSING PROGRAM\n");
                SHOULD_EXIT = 1;
                break;
            }
            // }
        }

        if (SHOULD_EXIT && PROXY_INIT_COMPLETE)
        {
            printf("Exiting readUdp loop due to should_exit flag.\n");
            break;
        }
    }
    return NULL;
}

uint64_t ntohll_custom(uint64_t netlonglong)
{
    uint64_t result = 0;
    uint8_t *pVal = (uint8_t *)&netlonglong;
    uint8_t *pResult = (uint8_t *)&result;
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    for (int i = 7; i >= 0; i--)
    {
        pResult[i] = pVal[i];
    }
#else
    for (int i = 0; i < 8; i++)
    {
        pResult[i] = pVal[i];
    }
#endif
    return result;
}

uint64_t htonll_custom(uint64_t hostlonglong)
{
    return ntohll_custom(hostlonglong);
}

int isOwnIp(struct sockaddr *addr)
{
    struct ifaddrs *interfaces, *iface;
    int is_own = 0;

    if (getifaddrs(&interfaces) == -1)
    {
        perror("getifaddrs");
        return -1;
    }

    for (iface = interfaces; iface != NULL; iface = iface->ifa_next)
    {
        if (iface->ifa_addr && iface->ifa_addr->sa_family == AF_INET)
        {
            if (memcmp(&((struct sockaddr_in *)iface->ifa_addr)->sin_addr,
                       &((struct sockaddr_in *)addr)->sin_addr,
                       sizeof(struct in_addr)) == 0)
            {
                is_own = 1;
                break;
            }
        }
    }

    freeifaddrs(interfaces);
    return is_own;
}

void sendSMCP1Heartbeat()
{
    sendSMCP1PositionChangedNotification(false);
}

void sendSMCP1PositionChangedNotification(bool multiUnicast)
{
    char buffer[100];
    smcp1_frame *smcp1Msg = (smcp1_frame *)buffer;

    smcp1Msg->version = 0x10;
    smcp1Msg->message_id = smcp1GetMessageId();
    smcp1Msg->receiver_id = SMCP1_ALL_CUS;
    smcp1Msg->type = SMCP1_NOTIFY_POSITION_CHANGED;
    smcp1Msg->sender_id = getSmcp1DeviceId();
    smcp1Msg->sub_blocks = 1;
    smcp1Msg->options = SMCP1_OPT_NOTIFY;
    smcp1Msg->sender_type = SCMP1_DEV_CATEGORY_UNDEFINED;

    smcp1_sb_header *pSbHdr = smcp1GetSubBlockHeader(smcp1Msg, 0);
    pSbHdr->type = SMCP1_DATA_INT32;
    pSbHdr->size = MCU_SYSTEM_ACT_MAX_COUNT;
    int32_t *pData = (int32_t *)smcp1GetSubBlockData(pSbHdr, 0);
    for (int axis = ACTUATOR_FIRST; axis <= ACTUATOR_LAST; axis++)
    {
        *pData++ = tmclGetPosition(axis, false);
    }

    if (multiUnicast)
        mcu_smcp10_sendToAll((const char *)smcp1Msg, sizeof(buffer));
    else
        sendUdpMessage(smcp1Msg, (struct sockaddr *)&r_addr);
}

mcu_error mcu_smcp10BroadcastStatusChangedNotification(uint32_t status)
{
    char buffer[100];
    smcp1_frame *smcp1Msg = (smcp1_frame *)buffer;

    smcp1Msg->version = 0x10;
    smcp1Msg->message_id = smcp1GetMessageId();
    smcp1Msg->receiver_id = SMCP1_ALL_CUS;
    smcp1Msg->type = SMCP1_NOTIFY_STATUS_CHANGED;
    smcp1Msg->sender_id = getSmcp1DeviceId();
    smcp1Msg->sub_blocks = 1;
    smcp1Msg->options = SMCP1_OPT_NOTIFY;
    smcp1Msg->sender_type = SCMP1_DEV_CATEGORY_UNDEFINED;

    smcp1_sb_header *pSbHdr = smcp1GetSubBlockHeader(smcp1Msg, 0);
    pSbHdr->type = SMCP1_DATA_UINT32;
    pSbHdr->size = 1;
    uint32_t *pData = (uint32_t *)smcp1GetSubBlockData(pSbHdr, 0);
    *pData = status;

    sendUdpMessage(smcp1Msg, (struct sockaddr *)&r_addr);

    return MCU_ERROR_NONE;
}

mcu_error sendTcmlRequest(const tmclReq *aReq, tmclResp *aResp)
{
    SpError error = SpErrorInvalid;
    uint8_t writeBuffer[TMCL_MSG_SIZE];

    tmclSerializeReq((tmclReq *)aReq, writeBuffer);

    pthread_mutex_lock(&serialMutex);
    int dataWrote = (int)write(serial_fd, (void *)writeBuffer, TMCL_MSG_SIZE);
    if (dataWrote == TMCL_MSG_SIZE)
    {
        uint8_t readBuffer[TMCL_MSG_SIZE];
        int bytes_read = (int)read(serial_fd, readBuffer, TMCL_MSG_SIZE);
        if (bytes_read == TMCL_MSG_SIZE)
        {
            if (aResp)
            {
                tmclDeserializeResp(readBuffer, aResp);
                if (tmclValidateResp(aResp))
                {
                    error = SpErrorOk;
                }
            }
            else
            {
                error = SpErrorOk;
            }
        }
        else
        {
            error = SpErrorRead;
            // if read fails, serial communication is lost, we exit the program
            printf("ERROR : SERIAL PORT READ COMMUNICATION FAILED, CLOSING PROGRAM\n");
            SHOULD_EXIT = 1;
        }
    }
    else
    {
        error = SpErrorWrite;
        // if write fails, serial communication is lost, we exit the program
        printf("ERROR : SERIAL PORT WRITE COMMUNICATION FAILED, CLOSING PROGRAM\n");
        SHOULD_EXIT = 1;
    }
    pthread_mutex_unlock(&serialMutex);

    return error == SpErrorOk ? MCU_ERROR_NONE : MCU_ERROR_GENERAL;
}

char *timeString()
{
    // Get current time
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    // Store time in a string
    static char timeStr[9];
    sprintf(timeStr, "%02d:%02d:%02d", t->tm_hour, t->tm_min, t->tm_sec);
    return timeStr;
}

mcu_error sendUdpMessage(const smcp1_frame *aSmcp1Msg, const struct sockaddr *addr)
{
    mcu_error error = MCU_ERROR_NONE;
    unsigned int frameSize = smcp1CalculateMessageSize(aSmcp1Msg);
    uint8_t *netMessage = mcuMalloc(frameSize);
    if (netMessage)
    {
        int bytesToSend = smcp1MessageHton((const uint8_t *)aSmcp1Msg, netMessage, (int)frameSize);
        pthread_mutex_lock(&udpMutex);
        int bytesSent = (int)sendto(udp_sock, netMessage, bytesToSend, 0, addr,
                                    sizeof(struct sockaddr));
        pthread_mutex_unlock(&udpMutex);
        if (bytesToSend != bytesSent)
        {
            if (errno == ECONNRESET)
            {
                error = MCU_ERROR_UDP_PEER_LOST;
            }
            else
            {
                error = MCU_ERROR_GENERAL;
            }
        }
        mcuFree(netMessage);
        netMessage = NULL;
    }
    return error;
}

mcu_error sendUdpData(const uint8_t *aData, int dataSize, const struct sockaddr *addr)
{
    mcu_error error = MCU_ERROR_NONE;
    if (aData)
    {
        pthread_mutex_lock(&udpMutex);
        int bytesSent = (int)sendto(udp_sock, aData, dataSize, 0, addr,
                                    sizeof(struct sockaddr));
        pthread_mutex_unlock(&udpMutex);
        if (dataSize != bytesSent)
        {
            if (errno == ECONNRESET)
            {
                error = MCU_ERROR_UDP_PEER_LOST;
            }
            else
            {
                error = MCU_ERROR_GENERAL;
            }
        }
    }
    else
    {
        error = MCU_ERROR_PARAMETER;
    }
    return error;
}

mcu_error processSmcp1Request(char *buffer, struct sockaddr *recv_addr, int bytes_read)
{
    mcu_error error = MCU_ERROR_NONE;
    if (smcp1ValidateMessage(buffer, bytes_read))
    {
        error = mcu_smcp10_handleRawEvent(buffer, bytes_read, recv_addr);
    }
    return error;
}
