/*-----------------------------------------------------------------------------
 *  File: port.c
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/
#include "port.h"
#include "constants.h"
#include "smcp1.h"
#include <errno.h>
#include <stdbool.h>

osStatus osMutexWait(osMutexId* mutex, int timeoutMs)
{
    UNUSED(timeoutMs);
    return pthread_mutex_lock(mutex);
}

osStatus osMutexRelease(osMutexId* mutex)
{
    return pthread_mutex_unlock(mutex);
}

osThreadId osThreadGetId(void)
{
    return pthread_self();
}

osStatus osThreadYield(void)
{
    return sched_yield() == -1 ? errno : 0;
}

uint32_t HAL_GetTick()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec * 1000 + ts.tv_nsec / 1000000;
}

osStatus osThreadSuspend (const osThreadId thread_id)
{
    UNUSED(thread_id);
    return osOK;
}

osStatus osThreadResume (const osThreadId thread_id)
{
    UNUSED(thread_id);
    return osOK;
}

#ifdef DEBUG_SMCPV1_PROXY_MALLOC
#include <malloc.h>
#include <string.h>

static pthread_mutex_t mallocMutex = PTHREAD_MUTEX_INITIALIZER;

void *mcuMalloc(size_t size)
{
    pthread_mutex_lock(&mallocMutex);
    void * memPtr = malloc(size);
    memset(memPtr, 0xff, size);
    pthread_mutex_unlock(&mallocMutex);
    return memPtr;
}

void mcuFree(void *ptr)
{
    pthread_mutex_lock(&mallocMutex);
    free(ptr);
    pthread_mutex_unlock(&mallocMutex);
}
#endif

