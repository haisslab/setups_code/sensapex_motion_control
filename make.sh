# Remove the build folder if it exists to force regeneration
[ -d "./build" ] && rm -rf ./build
# Create the build folder
mkdir ./build
# Configure the cmake build tree
sudo cmake -S ./sources -B ./build