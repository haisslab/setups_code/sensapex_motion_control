/*-----------------------------------------------------------------------------
 *  File: integration.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef INTEGRATION_H
#define INTEGRATION_H

#include "constants.h"
#include <stdint.h>
#include <stdbool.h>

uint16_t integrationGetDev();
uint16_t integrationSetDev(uint16_t devId);
char * integrationGetFirmwareVersion();
mcu_error integrationTakeLegacyStep(mcu_actuator act, int32_t gear, int32_t speed);
mcu_error integrationTakeStep(mcu_actuator act, int32_t speed_ums, int32_t step_nm);
mcu_error integrationInitActuator(mcu_actuator act);
mcu_error integrationGetPosition(mcu_actuator act, int *position, bool rawData);
mcu_error integrationGotoPosition(mcu_actuator act, int32_t position, uint32_t speed, bool async);
mcu_error integrationGetMemDriveTarget(mcu_actuator act, uint32_t memIndex, int32_t* position);
mcu_error integrationSetMemDriveTarget(mcu_actuator act, uint32_t memIndex, int32_t position);
mcu_error integrationStop(mcu_actuator act);

#endif
