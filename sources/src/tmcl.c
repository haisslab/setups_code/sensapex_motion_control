/*-----------------------------------------------------------------------------
 *  File: tmcl.c
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "tmcl.h"
#include "functions.h"
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

static int32_t gAxisPosition[MCU_SYSTEM_ACT_MAX_COUNT] = {0};

int32_t tmclGetPosition(uint8_t aAxis, bool rawData)
{
    int32_t retVal = gAxisPosition[aAxis];
    if (!rawData)
    {
        retVal *= TMCL_TO_SMCP_COORDINATE_MULTIPLIER;
    }
    return retVal;
}

int32_t tmclSetPosition(uint8_t aAxis, int32_t aPosition)
{
    gAxisPosition[aAxis] = aPosition;
    sendSMCP1PositionChangedNotification(true);
    return gAxisPosition[aAxis];
}

void tmclHeartBeatHandler()
{
    sendSMCP1Heartbeat();
}

uint8_t tmclCalculateCheckSumReq(const tmclReq *aReq)
{
    uint8_t checkSum = 0;
    checkSum += aReq->moduleAddr;
    checkSum += aReq->cmd;
    checkSum += aReq->type;
    checkSum += aReq->addr;
    for (int i = 0; i < 4; i++)
    {
        checkSum += (uint8_t)(aReq->data >> (8 * i));
    }
    return checkSum;
}

uint8_t tmclCalculateCheckSumResp(const tmclResp *aResp)
{
    uint8_t checkSum = 0;
    checkSum += aResp->replyAddr;
    checkSum += aResp->moduleAddr;
    checkSum += aResp->status;
    checkSum += aResp->cmd;
    for (int i = 0; i < 4; i++)
    {
        checkSum += (uint8_t)(aResp->data >> (8 * i));
    }
    return checkSum;
}

void tmclSerializeReq(tmclReq *aTmclReq, uint8_t *aBuffer)
{
    uint8_t *pData = aBuffer;

    *pData++ = aTmclReq->addr;
    *pData++ = aTmclReq->cmd;
    *pData++ = aTmclReq->type;
    *pData++ = aTmclReq->moduleAddr;
    for (int i = 3; i >= 0; i--)
    {
        *pData++ = (uint8_t)(aTmclReq->data >> (8 * i));
    }
    *pData++ = aTmclReq->checkSum;
    assert(pData - aBuffer == TMCL_MSG_SIZE);
}

void tmclDeserializeResp(uint8_t *aBuffer, tmclResp *aTmclResp)
{
    uint8_t *pData = aBuffer;
    aTmclResp->replyAddr = *pData++;
    aTmclResp->moduleAddr = *pData++;
    aTmclResp->status = *pData++;
    aTmclResp->cmd = *pData++;
    aTmclResp->data = 0;
    for (int i = 0; i < 4; i++)
    {
        aTmclResp->data = (aTmclResp->data << 8) | *pData++;
    }
    aTmclResp->checkSum = *pData++;
    assert(pData - aBuffer == TMCL_MSG_SIZE);
}

bool tmclValidateResp(const tmclResp *aResp)
{
    uint8_t checkSum = tmclCalculateCheckSumResp(aResp);
    bool valid = aResp->checkSum == checkSum;
    if (!valid)
    {
        printf("Checksum error: 0x%02x ", aResp->replyAddr);
        printf("0x%02x ", aResp->moduleAddr);
        printf("0x%02x ", aResp->status);
        printf("0x%02x ", aResp->cmd);
        printf("0x%04x ", aResp->data);
        printf("(checksum: 0x%02x expected: 0x%02x)", aResp->checkSum, checkSum);
        printf("\r\n");

        // TODO: Occurs occasionally (on USB), while containing seemingly valid data. Why?
        valid = true; // Ignore, for now.
    }
    return valid;
}
