/*-----------------------------------------------------------------------------
 *  File: smcp1.c
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "smcp1.h"
#include "constants.h"
#include "executor.h"
#include "functions.h"
#include "misc.h"
#include "port.h"
#include "version.h"
#include "integration.h"
#include <arpa/inet.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define SMCP10_PARAM_COUNT 25
#define SMCP10_PARAM_DYNAMIC_START 0x80
#define SMCP10_PARAM_READONLY_START 0x100
#define SMCP10_PARAM_DYNAMIC_END (SMCP10_PARAM_READONLY_START - 1)
#define SMCP10_PARAM_READONLY_END 0x200
#define SMCP10_PARAM_UNINITIALIZED -1

#define SMCP10_PEER_ADDR_CACHE_SIZE 5
#define SMCP10_PEER_ADDR_CACHE_TIMEOUT_MS 60000

typedef struct mcu_smcp1_peer_
{
    in_addr_t addr;
    uint32_t ts;
    uint16_t port;
} mcu_smcp10_peer;

static pthread_mutex_t peerMutex = PTHREAD_MUTEX_INITIALIZER;

// Pre-execution option handlers
static void handle_smcp10_pre_exec_opt_wait_trigger_1(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_priority(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_req_bcast(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_req_notify(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_req_resp(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_error(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_notify(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_ack(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_pre_exec_opt_req(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);

// Post-execution option handlers
static void handle_smcp10_post_exec_opt_priority(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_req_bcast(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_req_notify(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_req_resp(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_req_ack(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_error(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_notify(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_ack(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);
static void handle_smcp10_post_exec_opt_req(mcu_event_queue target, mcu_event *pEvent, mcu_event_status *pStatus);

// Command handlers
static int handle_smcp10_cmd_stop(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_get_parameter(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_set_parameter(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_get_version(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_get_info_text(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_take_step(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_take_legacy_step(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_store_mem(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_goto_mem(mcu_event_queue target, mcu_event *pEvent);
static int handle_smcp10_cmd_goto_mem_reverse(mcu_event_queue target, mcu_event *pEvent);
// Other functions
static char *smcpv10_compose_reply(mcu_event *pReqEvent, char *pData, int size, smcp1_sb_data_type type);
static int do_handle_smcp10_cmd_goto_mem(const mcu_event_queue target, mcu_event *pEvent, bool reverseOrder);
static mcu_error mcu_smcp10_set_default_params();

static uint32_t mcu_commonResolveActuatorAxisCount(const systemInfo *info);

static void mcu_smcp10SetLatestError(int32_t error);
static void smcp10_execute_error_handler(mcu_event *pEvent, int error);
static int mcu_smcp10SizeofTypeBase(smcp1_sb_data_type datatype);
static char *smcpv10_find_element(const mcu_event *pEvent, smcp1_mcu_event_element block);
static smcp1_frame *getSMCPv1Frame(const mcu_event *pEvent, smcp1_frame *pTarget);
static mcu_error mcu_smcp10_read_param(uint32_t paramId, int *value, bool refresh_cache);
static mcu_error mcu_smcp10_write_param(uint32_t paramID, int value, bool write_through);
static void mcu_smcp10_hton_frame(smcp1_frame *dest, smcp1_frame *source);

uint16_t smcp10_deviceID();

static int32_t latest_execute_error = 0;
static int smcp10_params_cache[SMCP10_PARAM_COUNT];
static mcu_smcp10_peer g_peer_address_cache[SMCP10_PEER_ADDR_CACHE_SIZE];

// Event attribute handler function pointer tables
// ... pre-execution
static pre_exec_handler smcp10_event_pre_exec_options_handlers[MCU_EVENT_OPTIONS_MAX_COUNT];
// ... post-execution
static post_exec_handler smcp10_event_post_exec_options_handlers[MCU_EVENT_OPTIONS_MAX_COUNT];

mcu_error mcu_smcp10_init()
{
    // Clear parameter cache
    memset(smcp10_params_cache, 0xFF, sizeof smcp10_params_cache);

    // Make sure that there is a meaningful default value for all main parameters
    mcu_error retval = mcu_smcp10_set_default_params();

    return retval;
}

static int smcp1SizeOfTypeBase(smcp1_sb_data_type datatype)
{
    int retVal;

    switch (datatype)
    {
    case SMCP1_DATA_VOID:
        retVal = sizeof(void);
        break;
    case SMCP1_DATA_UINT8:
    case SMCP1_DATA_UINT8_ARRAY_2:
    case SMCP1_DATA_UINT8_ARRAY_3:
    case SMCP1_DATA_UINT8_ARRAY_4:
    case SMCP1_DATA_CHAR_STRING:
        retVal = sizeof(uint8_t);
        break;
    case SMCP1_DATA_INT8:
    case SMCP1_DATA_INT8_ARRAY_2:
    case SMCP1_DATA_INT8_ARRAY_3:
    case SMCP1_DATA_INT8_ARRAY_4:
        retVal = sizeof(int8_t);
        break;
    case SMCP1_DATA_UINT16:
    case SMCP1_DATA_UINT16_ARRAY_2:
        retVal = sizeof(uint16_t);
        break;
    case SMCP1_DATA_INT16:
    case SMCP1_DATA_INT16_ARRAY_2:
        retVal = sizeof(int16_t);
        break;
    case SMCP1_DATA_UINT32:
        retVal = sizeof(uint32_t);
        break;
    case SMCP1_DATA_INT32:
        retVal = sizeof(int32_t);
        break;
    case SMCP1_DATA_UINT64:
        retVal = sizeof(uint64_t);
        break;
    case SMCP1_DATA_INT64:
        retVal = sizeof(int64_t);
        break;
    default:
        assert(false); // Unknown datatype. Assert it.
    }
    return retVal;
}

bool smcp1ValidateMessage(const char *p_data, int data_len)
{
    bool ret_val = false;
    if (data_len >= sizeof(smcp1_frame))
    {
        // Protocol version number matches
        if (*p_data == 0x10)
        {
            // TODO. Contains exactly as much as data it says - etc
            ret_val = true;
        }
    }
    return ret_val;
}

// network is 32 bit aligned
// host will be - what ever the current host is (32 bit or 64 bit most likely)
int smcp1MessageNtoh(const uint8_t *aSource, uint8_t *aTarget, int aSourceLen)
{
    int retVal = -1;

    if (aSourceLen >= sizeof(smcp1_frame))
    {
        const uint8_t *pSourceStart = aSource;

        // Frame
        smcp1_frame *pFrame = (smcp1_frame *)aTarget;

        pFrame->version = *(uint8_t *)aSource;
        aSource += sizeof(uint8_t);
        pFrame->sender_type = *(uint8_t *)aSource;
        aSource += sizeof(uint8_t);
        pFrame->receiver_id = ntohs(*(uint16_t *)aSource);
        aSource += sizeof(uint16_t);
        pFrame->sender_id = ntohs(*(uint16_t *)aSource);
        aSource += sizeof(uint16_t);
        pFrame->message_id = ntohs(*(uint16_t *)aSource);
        aSource += sizeof(uint16_t);
        pFrame->options = ntohl(*(uint32_t *)aSource);
        aSource += sizeof(uint32_t);
        pFrame->type = ntohs(*(uint16_t *)aSource);
        aSource += sizeof(uint16_t);
        pFrame->sub_blocks = ntohs(*(uint16_t *)aSource);
        aSource += sizeof(uint16_t);

        aTarget += sizeof(smcp1_frame);

        // Sub blocks
        // TODO. Buffer overflow checks
        for (int i = 0; i < pFrame->sub_blocks; i++)
        {

            smcp1_sb_header *pSbHeader;
            uint8_t *tmp = (uint8_t *)aTarget;
            pSbHeader = (smcp1_sb_header *)aTarget;
            pSbHeader->type = ntohs(*(uint16_t *)aSource);
            aSource += sizeof(uint16_t);
            pSbHeader->size = ntohs(*(uint16_t *)aSource);
            aSource += sizeof(uint16_t);

            aTarget = tmp + sizeof(smcp1_sb_header);

            // Process sub block payload.
            int elementSize = smcp1SizeOfTypeBase(pSbHeader->type);
            for (int j = 0; j < pSbHeader->size; j++)
            {
                switch (elementSize)
                {
                case sizeof(uint8_t):
                    *(aTarget) = *(aSource);
                    break;
                case sizeof(uint16_t):
                    *(uint16_t *)aTarget = ntohs(*(uint16_t *)aSource);
                    break;
                case sizeof(uint32_t):
                    *(uint32_t *)aTarget = ntohl(*(uint32_t *)aSource);
                    break;
                case sizeof(uint64_t):
                    *(uint64_t *)aTarget = ntohll_custom(*(uint64_t *)aSource);
                    break;
                default:
                    assert(false);
                }
                aSource += elementSize;
                aTarget += elementSize;
            }
            aTarget += elementSize + ((elementSize * pSbHeader->size) % sizeof(int)); // Align to native word size
        }

        retVal = (int)(aSource - pSourceStart);
    }
    return retVal;
}

int smcp1MessageHton(const uint8_t *aSource, uint8_t *aTarget, int aSourceLen)
{
    int retVal = -1;

    if (aSourceLen >= sizeof(smcp1_frame))
    {
        const uint8_t *pSourceStart = aSource;
        const uint8_t *pTargetStart = aTarget;

        // Frame
        smcp1_frame *pFrame = (smcp1_frame *)aSource;
        uint16_t subBlockCnt = pFrame->sub_blocks;

        *(uint8_t *)aTarget = pFrame->version;
        aTarget += sizeof(uint8_t);
        *(uint8_t *)aTarget = pFrame->sender_type;
        aTarget += sizeof(uint8_t);
        *(uint16_t *)aTarget = htons(pFrame->receiver_id);
        aTarget += sizeof(uint16_t);
        *(uint16_t *)aTarget = htons(pFrame->sender_id);
        aTarget += sizeof(uint16_t);
        *(uint16_t *)aTarget = htons(pFrame->message_id);
        aTarget += sizeof(uint16_t);
        *(uint32_t *)aTarget = htonl(pFrame->options);
        aTarget += sizeof(uint32_t);
        *(uint16_t *)aTarget = htons(pFrame->type);
        aTarget += sizeof(uint16_t);
        *(uint16_t *)aTarget = htons(pFrame->sub_blocks);
        aTarget += sizeof(uint16_t);

        aSource += sizeof(smcp1_frame);

        // Sub blocks
        // TODO. Buffer overflow checks
        for (int i = 0; i < subBlockCnt; i++)
        {

            smcp1_sb_header *pSbHeader;
            pSbHeader = (smcp1_sb_header *)aSource;

            uint16_t sbDataType = pSbHeader->type;
            uint16_t sbDataSize = pSbHeader->size;
            uint8_t *tmp = (uint8_t *)aSource;

            *(uint16_t *)aTarget = htons(pSbHeader->type);
            aTarget += sizeof(uint16_t);
            *(uint16_t *)aTarget = htons(pSbHeader->size);
            aTarget += sizeof(uint16_t);

            aSource = tmp + sizeof(smcp1_sb_header);

            // Process sub block payload.
            int elementSize = smcp1SizeOfTypeBase(sbDataType);
            for (int j = 0; j < sbDataSize; j++)
            {
                switch (elementSize)
                {
                case sizeof(uint8_t):
                    *(aTarget) = *(aSource);
                    break;
                case sizeof(uint16_t):
                    *(uint16_t *)aTarget = htons(*(uint16_t *)aSource);
                    break;
                case sizeof(uint32_t):
                    *(uint32_t *)aTarget = htonl(*(uint32_t *)aSource);
                    break;
                case sizeof(uint64_t):
                    *(uint64_t *)aTarget = htonll_custom(*(uint64_t *)aSource);
                    break;
                default:
                    assert(false);
                }
                aSource += elementSize;
                aTarget += elementSize;
            }
        }

        assert(((int)(aSource - pSourceStart)) == ((int)(aTarget - pTargetStart)));

        retVal = (int)(aTarget - pTargetStart);
    }
    return retVal;
}

char *smcp1FindElement(const char *pEvent, smcp1_element block, int sbIndex)
{
    char *retVal = (char *)pEvent;
    int offset;
    switch (block)
    {
    /*
    case SCMP1_FRAME_ELEM:
        offset = 0;
        break;
        */
    case SCMP1_SUB_BLOCKS_ELEM:
        offset = sizeof(smcp1_frame);
        smcp1_sb_header *pSbHeader = (smcp1_sb_header *)(pEvent + offset);
        // FIXME. Data alignment with multiple sub blocks
        for (int i = 0; i < sbIndex; i++)
        {
            offset += (int)SMCP1_SUB_BLOCK_HEADER_SIZE + (pSbHeader->size * smcp1SizeOfTypeBase(pSbHeader->type));
            pSbHeader = (smcp1_sb_header *)(pEvent + offset);
        }
        break;
    default:
        assert(false); // Unknown parameter. Assert it.
    }
    retVal += offset;
    return retVal;
}

smcp1_sb_header *smcp1GetSubBlockHeader(const smcp1_frame *pEvent, int sbIndex)
{
    smcp1_sb_header *retVal = NULL;
    char *pSb = smcp1FindElement((const char *)pEvent, SCMP1_SUB_BLOCKS_ELEM, sbIndex);
    if (pSb != NULL)
    {
        retVal = (smcp1_sb_header *)pSb;
    }
    return retVal;
}

void *smcp1GetSubBlockData(smcp1_sb_header *pSbHdr, uint32_t index)
{
    int itemSize = smcp1SizeOfTypeBase(pSbHdr->type);
    void *pSbDataBlock = (void *)pSbHdr + sizeof(smcp1_sb_header);
    void *retVal = (void *)pSbDataBlock + (index * itemSize);
    return retVal;
}

static void *smcpv10_get_data(smcp1_sb_header *pSbHdr, char *pSbDataBlock, uint32_t index)
{
    int itemSize = mcu_smcp10SizeofTypeBase(pSbHdr->type);
    void *retval = (void *)pSbDataBlock + (index * itemSize);
    return retval;
}

static int mcu_smcp10NbrOfDataElements(smcp1_sb_data_type datatype)
{
    int retval;

    switch (datatype)
    {
    case SMCP1_DATA_VOID:
    case SMCP1_DATA_UINT8:
    case SMCP1_DATA_INT8:
    case SMCP1_DATA_CHAR_STRING:
    case SMCP1_DATA_UINT16:
    case SMCP1_DATA_INT16:
    case SMCP1_DATA_UINT32:
    case SMCP1_DATA_INT32:
    case SMCP1_DATA_UINT64:
    case SMCP1_DATA_INT64:
        retval = 1;
        break;
    case SMCP1_DATA_UINT8_ARRAY_2:
    case SMCP1_DATA_INT8_ARRAY_2:
    case SMCP1_DATA_UINT16_ARRAY_2:
    case SMCP1_DATA_INT16_ARRAY_2:
        retval = 2;
        break;
    case SMCP1_DATA_UINT8_ARRAY_3:
    case SMCP1_DATA_INT8_ARRAY_3:
        retval = 3;
        break;
    case SMCP1_DATA_UINT8_ARRAY_4:
    case SMCP1_DATA_INT8_ARRAY_4:
        retval = 4;
        break;
    default:
        retval = 0;
        assert(0); // Unknown datatype. Assert it.
        break;
    }
    return retval;
}

static int mcu_smcp10SizeofSbPayload(smcp1_sb_header *pSb)
{
    int payloadSize;
    // base datatype size...
    int typeSize = mcu_smcp10SizeofTypeBase(pSb->type);
    // ... multiplied by the number of items in array
    typeSize *= mcu_smcp10NbrOfDataElements(pSb->type);
    // ... is the actual data size in bytes
    payloadSize = pSb->size * typeSize;
    // ... but since data must be 32 bit aligned
    // ... we calculate the size of end paddings also.
    payloadSize += payloadSize % 4;

    return payloadSize;
}

static mcu_error mcu_smcpv1_get_sub_block_data(const mcu_event *pEvent, int sbIndex,
                                               smcp1_sb_header **pDestSb, char **pDestSbData)
{
    mcu_error retval = MCU_ERROR_NOT_FOUND;
    smcp1_sb_header *pSb = (smcp1_sb_header *)
        smcpv10_find_element(pEvent, SCMP1_SUBBLOCK_ELEM);
    int i;

    assert(pEvent->frame.sub_blocks >= sbIndex);

    for (i = 0; i < pEvent->frame.sub_blocks; i++)
    {
        int payloadSize = mcu_smcp10SizeofSbPayload(pSb);

        if (i == sbIndex)
        {
            // We found the target sub-block. Set return values and stop.
            *pDestSb = pSb;
            *pDestSbData = (char *)((char *)pSb + sizeof(smcp1_sb_header));
            retval = MCU_ERROR_NONE;
            break;
        }
        pSb = (smcp1_sb_header *)((void *)pSb + sizeof(smcp1_sb_header) + payloadSize);
    }
    return retval;
}

const char *smcp1GetVersion()
{
    static char versionBuffer[15] = {'\0'};
    if (versionBuffer[0] == '\0')
    {
        sprintf(versionBuffer, "%d.%d.%d.%d", SMCP_PROXY_VERSION_MAJOR, SMCP_PROXY_VERSION_YEAR,
                SMCP_PROXY_VERSION_WEEK, SMCP_PROXY_VERSION_DAY_AND_BUILD);
    }
    return versionBuffer;
}

uint16_t smcp1GetMessageId()
{
    static uint16_t msgIndex = 0;
    uint16_t retVal = msgIndex++;
    return retVal;
}

unsigned int smcp1CalculateMessageSize(const smcp1_frame *pFrame)
{
    unsigned int retVal = sizeof(smcp1_frame);
    for (int i = 0; i < pFrame->sub_blocks; i++)
    {
        smcp1_sb_header *pSbHeader = smcp1GetSubBlockHeader(pFrame, i);
        retVal += sizeof(smcp1_sb_header) + (pSbHeader->size * smcp1SizeOfTypeBase(pSbHeader->type));
    }
    return retVal;
}

uint16_t getSmcp1DeviceId()
{
    return (uint16_t)smcp10_deviceID();
}

/********************* SMCP10 port *************************/

static void initSMCP10_PreExecutionHandler(pre_exec_handler *handler)
{
    *handler = &handle_smcp10_pre_exec_opt_req;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_ack;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_notify;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_error;
    handler++;
    *handler = NULL; // SMCP1_OPT_REQ_ACK;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_req_resp;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_req_notify;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_req_bcast;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_priority;
    handler++;
    *handler = &handle_smcp10_pre_exec_opt_wait_trigger_1;
}
static mcu_error mcu_smcp10_resolve_target(const mcu_event *pEvent,
                                           mcu_event_queue *dest)
{
    mcu_error retval = MCU_ERROR_NONE;

    switch (pEvent->frame.type)
    {
    case SMCP1_CMD_STOP:
    case SMCP1_GET_VERSION:
    case SMCP1_GET_INFO_TEXT:
    case SMCP1_CMD_TAKE_STEP:
    case SMCP1_CMD_TAKE_LEGACY_STEP:
    case SMCP1_GET_PARAMETER:
    case SMCP1_SET_PARAMETER:
    case SMCP1_CMD_STORE_MEM:
    case SMCP1_CMD_GOTO_MEM:
        *dest = MCU_EVENT_QUEUE_COMM;
        break;
    default:
        retval = MCU_ERROR_NOT_SUPPORTED;
        break;
    }

    return retval;
}

cmd_exec_handler SMCP10_ResolveCmdHandler(int cmd)
{
    cmd_exec_handler retval;

    switch (cmd)
    {
    case SMCP1_CMD_STOP:
        retval = handle_smcp10_cmd_stop;
        break;
    case SMCP1_GET_PARAMETER:
        retval = handle_smcp10_cmd_get_parameter;
        break;
    case SMCP1_SET_PARAMETER:
        retval = handle_smcp10_cmd_set_parameter;
        break;
    case SMCP1_GET_VERSION:
        retval = handle_smcp10_cmd_get_version;
        break;
    case SMCP1_CMD_STORE_MEM:
        retval = handle_smcp10_cmd_store_mem;
        break;
    case SMCP1_CMD_GOTO_MEM:
        retval = handle_smcp10_cmd_goto_mem;
        break;
    case SMCP1_CMD_GOTO_MEM_REVERSE:
        retval = handle_smcp10_cmd_goto_mem_reverse;
        break;
    case SMCP1_GET_INFO_TEXT:
        retval = handle_smcp10_cmd_get_info_text;
        break;
    case SMCP1_CMD_TAKE_STEP:
        retval = handle_smcp10_cmd_take_step;
        break;
    case SMCP1_CMD_TAKE_LEGACY_STEP:
        retval = handle_smcp10_cmd_take_legacy_step;
        break;

    default:
        retval = NULL;
        break;
    }
    return retval;
}

static void initSMCP10_PostExecutionHandler(post_exec_handler *handler)
{
    *handler = &handle_smcp10_post_exec_opt_req;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_ack;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_notify;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_error;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_req_ack;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_req_resp;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_req_notify;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_req_bcast;
    handler++;
    *handler = &handle_smcp10_post_exec_opt_priority;
    handler++;
    *handler = NULL; // SMCP1_OPT_WAIT_TRIGGER_1
}

void mcu_smcp10SetLatestError(int32_t error)
{
    latest_execute_error = error;
}

void smcp10_execute_error_handler(mcu_event *pEvent, int error)
{
    pEvent->frame.options |= SMCP1_OPT_ERROR;
    mcu_smcp10SetLatestError((int32_t)error);
}

static void handle_smcp10_pre_exec_opt_wait_trigger_1(const mcu_event_queue target,
                                                      mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_pre_exec_opt_priority(const mcu_event_queue target,
                                                mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_pre_exec_opt_req_bcast(const mcu_event_queue target,
                                                 mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pStatus);

    pEvent->replyaddr.sin_family = AF_INET;
    inet_pton(AF_INET, SMCP1_BROADCAST_ADDR, &pEvent->replyaddr.sin_addr);
    pEvent->replyaddr.sin_port = htons(pEvent->replyaddr.sin_port);

    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_pre_exec_opt_req_notify(const mcu_event_queue target,
                                                  mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    //  MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_pre_exec_opt_req_resp(const mcu_event_queue target,
                                                mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    //  MCUTRACE(("%s\n", __func__));
}
static void handle_smcp10_pre_exec_opt_req_ack(const mcu_event_queue target,
                                               mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pStatus);

    smcp1_frame reply;
    // Build a reply frame without Sub-blocks
    smcp1_frame *origFrame = getSMCPv1Frame(pEvent, &reply);

    // Switch the content of sender id and received id fields
    reply.receiver_id = origFrame->sender_id;
    reply.sender_id = smcp10_deviceID();
    // reply sub-blocks included.
    reply.sub_blocks = 0;

    // Set ACK bit on (only ACK)
    reply.options = SMCP1_OPT_ACK;

    // Change the data format to network format (big-endian)
    mcu_smcp10_hton_frame(&reply, &reply);

    // Send the data
    mcu_error status = sendUdpData((const uint8_t *)&reply, sizeof(smcp1_frame),
                                   (const struct sockaddr *)&pEvent->replyaddr);

    assert(status == MCU_ERROR_NONE || status == MCU_ERROR_UDP_PEER_LOST);
    UNUSED(status);
}

static void handle_smcp10_pre_exec_opt_error(const mcu_event_queue target,
                                             mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_pre_exec_opt_notify(const mcu_event_queue target,
                                              mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    //  MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_pre_exec_opt_ack(const mcu_event_queue target,
                                           mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_pre_exec_opt_req(const mcu_event_queue target,
                                           mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_post_exec_opt_priority(const mcu_event_queue target,
                                                 mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_post_exec_opt_req_bcast(const mcu_event_queue target,
                                                  mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_post_exec_opt_req_notify(const mcu_event_queue target,
                                                   mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);

    if (mcu_executorIsEventDone(pStatus))
    {
        int dataSize = sizeof(smcp1_frame) + sizeof(smcp1_sb_header) + sizeof(uint32_t);
        char buffer[dataSize];

        smcp1_frame *pReply = (smcp1_frame *)&buffer[0];
        smcp1_frame *origFrame = getSMCPv1Frame(pEvent, pReply);
        assert(origFrame);

        // Switch the content of sender id and received id fields
        pReply->receiver_id = origFrame->sender_id;
        pReply->sender_id = smcp10_deviceID();

        // Set frame options. This is a notification.
        pReply->options = SMCP1_OPT_NOTIFY;

        // Resolve a notification type.
        uint16_t type;

        switch (pEvent->rootType)
        {

        case SMCP1_CMD_INIT_ZERO:
            type = SMCP1_NOTIFY_GOTO_ZERO_COMPLETED;
            break;
        case SMCP1_CMD_GOTO_MEM:
        case SMCP1_CMD_GOTO_POS:
            type = SMCP1_NOTIFY_GOTO_POS_COMPLETED;
            break;
        case SMCP1_CMD_CALIBRATE:
            type = SMCP1_NOTIFY_CALIBRATE_COMPLETED;
            break;
        case SMCP1_CMD_DRIVE_LOOP:
            type = SMCP1_NOTIFY_DRIVE_LOOP_COMPLETED;
            break;
        default:
            type = pEvent->rootType;
            break;
        }

        // Set the resolved type.
        pReply->type = type;

        // Validate that reply type is from the external range.
        assert(pReply->type < 2000);

        // One sub block must be included
        // ... sb header
        pReply->sub_blocks = 1;
        smcp1_sb_header *pSbHdr = (smcp1_sb_header *)((void *)pReply + sizeof(smcp1_frame));
        pSbHdr->size = 1;
        pSbHdr->type = SMCP1_DATA_UINT32;

        // ... sb data
        uint32_t *pData = (uint32_t *)((void *)pSbHdr + sizeof(smcp1_sb_header));
        *pData = pStatus->error;

        // Change the data format to network format (big-endian)
        mcu_smcp10_hton_frame(pReply, pReply);

        // Send the response data
        mcu_error status = sendUdpData((const uint8_t *)pReply, dataSize,
                                       (const struct sockaddr *)&pEvent->replyaddr);

        assert(status == MCU_ERROR_NONE || status == MCU_ERROR_UDP_PEER_LOST);
        UNUSED(status);
    }
}

static void handle_smcp10_post_exec_opt_req_resp(const mcu_event_queue target,
                                                 mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);

    if (pEvent->frame.options & SMCP1_OPT_REQ)
    {
        mcu_error status = MCU_ERROR_NONE;
        if (mcu_executorIsEventDone(pStatus))
        {
            char *pRespData = NULL;
            int respLen = 0;
            smcp1_frame defaultreply;

            if (pStatus->pDataDeferred)
            {
                assert(pEvent->pData1 == NULL);
                pEvent->pData1 = (char *)pStatus->pDataDeferred;
                // Ownership transferred
                status = mcu_executorStatusInsertPayload(pStatus, NULL);
            }

            if (status == MCU_ERROR_NONE)
            {
                if (pEvent->pData1)
                {
                    // We have a custom message for reply. Use it.
                    // The first 32 bit indicates the length of actual data
                    int *ptrExtData = (int *)(pEvent->pData1);
                    respLen = *ptrExtData;
                    ptrExtData++;
                    // ... the actual data starts after length block
                    pRespData = (char *)ptrExtData;
                }
                else
                {
                    // Default reply handler
                    smcp1_frame *origFrame = getSMCPv1Frame(pEvent, &defaultreply);
                    assert(origFrame);

                    // Switch the content of sender id and received id fields
                    defaultreply.receiver_id = origFrame->sender_id;
                    defaultreply.sender_id = smcp10_deviceID();

                    // No sub-blocks included.
                    defaultreply.sub_blocks = 0;

                    // Use rootType for all replies:
                    defaultreply.type = pEvent->rootType;

                    // Validate that reply type is from the external range.
                    assert(defaultreply.type < 2000);

                    // Modify frame options
                    // ... No ack needed + this is a response (switch off a req flag).
                    defaultreply.options &= (~(SMCP1_OPT_REQ_ACK | SMCP1_OPT_REQ | SMCP1_OPT_REQ_RESP | SMCP1_OPT_REQ_NOTIFY));

                    // Change the data format to network format (big-endian)
                    mcu_smcp10_hton_frame(&defaultreply, &defaultreply);

                    // Set send pointers
                    pRespData = (char *)&defaultreply;
                    respLen = sizeof(smcp1_frame);
                }

                // Send the response data
                status = sendUdpData((const uint8_t *)pRespData, respLen, (const struct sockaddr *)&pEvent->replyaddr);
            }
        }
        else
        {
            if (pEvent->pData1)
            {
                // Save the payload
                mcu_error status = mcu_executorStatusInsertPayload(pEvent->pStatus, pEvent->pData1);
                if (status == MCU_ERROR_NONE)
                {
                    // Ownership transferred
                    pEvent->pData1 = NULL;
                }
            }
        }
        assert(status == MCU_ERROR_NONE || status == MCU_ERROR_UDP_PEER_LOST);
    }
}

static void handle_smcp10_post_exec_opt_req_ack(const mcu_event_queue target,
                                                mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    //  MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_post_exec_opt_error(const mcu_event_queue target,
                                              mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_post_exec_opt_notify(const mcu_event_queue target,
                                               mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    //  MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_post_exec_opt_ack(const mcu_event_queue target,
                                            mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

static void handle_smcp10_post_exec_opt_req(const mcu_event_queue target,
                                            mcu_event *pEvent, mcu_event_status *pStatus)
{
    UNUSED(target);
    UNUSED(pEvent);
    UNUSED(pStatus);
    MCUTRACE(("%s\n", __func__));
}

void SMCP10_RegisterExecutor(void *info)
{
    execHandlerInfo *info2 = (execHandlerInfo *)info;
    // Initialize pre&posthandler function tables
    initSMCP10_PreExecutionHandler(smcp10_event_pre_exec_options_handlers);
    initSMCP10_PostExecutionHandler(smcp10_event_post_exec_options_handlers);

    info2->preCmdHandlers = smcp10_event_pre_exec_options_handlers;
    info2->cmdResolver = SMCP10_ResolveCmdHandler;
    info2->execErrorHandler = smcp10_execute_error_handler;
    info2->postCmdHandlers = smcp10_event_post_exec_options_handlers;
}

smcp1_frame *getSMCPv1Frame(const mcu_event *pEvent, smcp1_frame *pTarget)
{
    smcp1_frame *pSource = (smcp1_frame *)smcpv10_find_element(pEvent, SCMP1_FRAME_ELEM);

    if (pTarget)
    {
        // Make a copy of the original frame
        memcpy(pTarget, pSource, sizeof(smcp1_frame));
    }
    return pSource;
}

char *smcpv10_compose_reply(mcu_event *pReqEvent, char *pData, int size,
                            smcp1_sb_data_type type)
{
    int typeSize = mcu_smcp10SizeofTypeBase(type);

    int respSize = (int)(sizeof(uint32_t) + sizeof(smcp1_frame) +
                         sizeof(smcp1_sb_header) + (size * typeSize));

    char *retval = mcuMalloc(respSize);

    if (retval)
    {
        // compose a message
        // Data size is the very first element of extension data block
        uint32_t *tmp = (uint32_t *)retval;
        *tmp = respSize - sizeof(uint32_t); // The size of actual data element
        tmp++;

        // The actual data goes right after the size
        smcp1_frame *pResp = (smcp1_frame *)tmp;
        smcp1_frame *pReq = (smcp1_frame *)smcpv10_find_element(pReqEvent, SCMP1_FRAME_ELEM);

        smcp1_sb_header *pRespSb = (smcp1_sb_header *)((void *)pResp + sizeof(smcp1_frame));

        // Make a raw copy
        memcpy(pResp, pReq, sizeof(smcp1_frame));
        // Set senderId and respId
        pResp->receiver_id = pReq->sender_id;
        pResp->sender_id = smcp10_deviceID();
        // Set device type
        pResp->sender_type = smcp10_deviceType();
        // Set options
        pResp->options = 0;
        // Set command type. Use the root type for all replies.
        pResp->type = pReqEvent->rootType;
        // Set the sub-block
        // ... sb header
        pResp->sub_blocks = 1;
        pRespSb->size = size;
        pRespSb->type = type;

        // ... sb data
        char *pRespSbData = (char *)((void *)pRespSb + sizeof(smcp1_sb_header));
        memcpy(pRespSbData, pData, typeSize * size);

        // Finally. Convert the data from host to network format
        mcu_smcp10_hton_frame(pResp, pResp);
    }
    return retval;
}

static void mcu_smcpv10_copyPeer(mcu_smcp10_peer *dest, const mcu_smcp10_peer *src)
{
    pthread_mutex_lock(&peerMutex);
    memcpy(dest, src, sizeof(mcu_smcp10_peer));
    pthread_mutex_unlock(&peerMutex);
}

static mcu_error mcu_smcpv10_getPeer(const uint32_t index, mcu_smcp10_peer *peer)
{
    mcu_error retVal = MCU_ERROR_NONE;
    if (index < SMCP10_PEER_ADDR_CACHE_SIZE)
    {
        mcu_smcpv10_copyPeer(peer, &g_peer_address_cache[index]);
    }
    else
    {
        retVal = MCU_ERROR_OVERFLOW;
    }
    return retVal;
}

static mcu_error mcu_smcpv10_setPeer(mcu_smcp10_peer *peer, const uint32_t index)
{
    mcu_error retVal = MCU_ERROR_NONE;
    if (index < SMCP10_PEER_ADDR_CACHE_SIZE)
    {
        mcu_smcpv10_copyPeer(&g_peer_address_cache[index], peer);
    }
    else
    {
        retVal = MCU_ERROR_OVERFLOW;
    }
    return retVal;
}

static void mcu_smcp10_updatePeerAddressCache(const in_addr_t *addr, uint16_t port)
{
    uint32_t ts = HAL_GetTick();
    uint32_t oldest_ts = ts;
    int32_t entry_index = -1;

    bool updated = false;

    mcu_smcp10_peer tmp_peer;
    mcu_smcp10_peer *peer = &tmp_peer;

    for (uint32_t i = 0; i < SMCP10_PEER_ADDR_CACHE_SIZE; i++)
    {

        // Detect the oldest peer address, it will be overwritten
        // with a new one if all address slots are in use.
        // This will also detect the first empty slot
        mcu_smcpv10_getPeer(i, peer);

        if (peer->ts < oldest_ts)
        {
            oldest_ts = peer->ts;
            entry_index = i;
        }

        // An empty slot
        if (!peer->ts)
        {
            continue;
        }

        // Timer wraparound handling
        if (peer->ts > ts)
        {
            peer->ts = ts;
        }

        // Update the existing entry in cache
        if (peer->addr == *addr && peer->port == port)
        {
            peer->ts = ts;
            entry_index = i;
            updated = true;
            break;
        }
    }

    // Add new entry in cache
    if (!updated)
    {
        peer->ts = ts;
        peer->port = port;
        peer->addr = *addr;
    }

    // Store an entry in the cache
    mcu_smcpv10_setPeer(peer, entry_index);
}

mcu_error mcu_smcp10_sendToAll(const char *data, unsigned int datasize)
{
    mcu_error status, retval = MCU_ERROR_NONE;

    mcu_smcp10_peer tmp_peer;
    mcu_smcp10_peer *peer = &tmp_peer;

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET; // IPv4

    for (uint32_t i = 0; i < SMCP10_PEER_ADDR_CACHE_SIZE; i++)
    {

        mcu_smcpv10_getPeer(i, peer);

        if (!peer->ts)
            continue;

        // Check if peer is lost by not got any message from it for a long time.
        if (mcu_commonIsExpired(HAL_GetTick(), peer->ts,
                                SMCP10_PEER_ADDR_CACHE_TIMEOUT_MS))
        {
            memset(peer, 0, sizeof(mcu_smcp10_peer));
            mcu_smcpv10_setPeer(peer, i);
            continue;
        }

        addr.sin_addr.s_addr = peer->addr;
        addr.sin_port = peer->port;
        sendUdpMessage((const smcp1_frame *)data, (struct sockaddr *)&addr);

        // Clear address if peer is lost
        if (status == MCU_ERROR_UDP_PEER_LOST)
        {
            memset(peer, 0, sizeof(mcu_smcp10_peer));
            mcu_smcpv10_setPeer(peer, i);
        }
        if (status != MCU_ERROR_NONE)
            retval = status;
    }

    return retval;
}

uint16_t smcp10_deviceID()
{
    int retval;
    mcu_error status = mcu_smcp10_read_param(SMCP1_PARAM_DEV_ID, &retval, false);
    if (status == MCU_ERROR_NONE)
    {
        return (uint16_t)retval;
    }
    else
    {
        return 0;
    }
    return retval;
}

char *smcpv10_find_element(const mcu_event *pEvent,
                           smcp1_mcu_event_element block)
{
    char *retval = (char *)pEvent;

    switch (block)
    {
    case SCMP1_MCU_EVENT_ELEM:
        break;
    case SCMP1_FRAME_ELEM:
        retval = (char *)&pEvent->frame;
        break;
    case SCMP1_SUBBLOCK_ELEM:
        retval = (char *)pEvent + sizeof(mcu_event);
        break;
    default:
        assert(false); // Unknown parameter. Assert it.
    }
    return retval;
}

int mcu_smcp10SizeofTypeBase(smcp1_sb_data_type datatype)
{
    int retval;

    switch (datatype)
    {
    case SMCP1_DATA_VOID:
        retval = sizeof(void);
        break;
    case SMCP1_DATA_UINT8:
    case SMCP1_DATA_UINT8_ARRAY_2:
    case SMCP1_DATA_UINT8_ARRAY_3:
    case SMCP1_DATA_UINT8_ARRAY_4:
    case SMCP1_DATA_CHAR_STRING:
        retval = sizeof(uint8_t);
        break;
    case SMCP1_DATA_INT8:
    case SMCP1_DATA_INT8_ARRAY_2:
    case SMCP1_DATA_INT8_ARRAY_3:
    case SMCP1_DATA_INT8_ARRAY_4:
        retval = sizeof(int8_t);
        break;
    case SMCP1_DATA_UINT16:
    case SMCP1_DATA_UINT16_ARRAY_2:
        retval = sizeof(uint16_t);
        break;
    case SMCP1_DATA_INT16:
    case SMCP1_DATA_INT16_ARRAY_2:
        retval = sizeof(int16_t);
        break;
    case SMCP1_DATA_UINT32:
        retval = sizeof(uint32_t);
        break;
    case SMCP1_DATA_INT32:
        retval = sizeof(int32_t);
        break;
    case SMCP1_DATA_UINT64:
        retval = sizeof(uint64_t);
        break;
    case SMCP1_DATA_INT64:
        retval = sizeof(int64_t);
        break;
    default:
        retval = 0;
        assert(false); // Unknown datatype. Assert it.
        break;
    }
    return retval;
}

smcp1_device_category smcp10_deviceType()
{
    return SCMP1_DEV_CATEGORY_UNDEFINED;
}

void mcu_smcp10_hton_frame(smcp1_frame *dest, smcp1_frame *source)
{
    smcp1MessageHton((const uint8_t *)source, (uint8_t *)dest, sizeof(smcp1_frame));
}

void mcu_smcp10_ntoh_frame(smcp1_frame *dest, smcp1_frame *source)
{
    smcp1MessageNtoh((const uint8_t *)source, (uint8_t *)dest, sizeof(smcp1_frame));
}

static bool mcu_smcp10_accept(const smcp1_frame *pFrame)
{
    bool retval = false;

    uint16_t ownId = smcp10_deviceID();

    if (pFrame->receiver_id == ownId ||
        (pFrame->receiver_id == SMCP1_ALL_DEVICES &&
         pFrame->sender_id != ownId))
    {
        retval = true;
    }

    return retval;
}

mcu_error mcu_smcp10_handleRawEvent(char *rawData, int rawDataLen, struct sockaddr *recv_addr)
{
    mcu_error retval = MCU_ERROR_NO_MEMORY;

    // Data block contains a smcpv1 header + subblock.
    // mcu_event has a few custom fields also.
    // Allocate a single heap block for all this stuff.
    int rawEventSize = rawDataLen;
    int eventSize = rawEventSize + (int)(sizeof(mcu_event) + (sizeof(smcp1_sb_header) * 2)); // An estimation

    mcu_event *pEvent = mcuMalloc(eventSize);

    if (pEvent)
    {
        mcu_event_queue dest;

        // Resolve a pointer location for smcpv1 frame ...
        smcp1_frame *pSmcpv1_frame = (smcp1_frame *)smcpv10_find_element(pEvent, SCMP1_FRAME_ELEM);

        // ... Convert the event format to host format
        mcu_smcp10_ntoh_frame(pSmcpv1_frame, (smcp1_frame *)rawData);

        // ... Check the recipient
        if (mcu_smcp10_accept(pSmcpv1_frame))
        {

            // ... Add the content of pure mcu_event specific fields
            memcpy(&pEvent->replyaddr, recv_addr, sizeof(struct sockaddr));
            pEvent->pData1 = NULL;
            pEvent->pStatus = NULL;
            pEvent->extra2 = 0;

            // ... Set the priority
            if (pEvent->frame.options & SMCP1_OPT_PRIORITY)
                pEvent->priority = MCU_EVENT_PRIORITY_HIGH;
            else
                pEvent->priority = MCU_EVENT_PRIORITY_NORMAL;

            // ... Resolve a target queue.
            retval = mcu_smcp10_resolve_target(pEvent, &dest);

            if (retval == MCU_ERROR_NONE)
            {

                // This is the root event. Set the type.
                pEvent->rootType = pEvent->frame.type;

                // Send an ACK immediately
                if (pEvent->frame.options & SMCP1_OPT_REQ_ACK)
                {
                    handle_smcp10_pre_exec_opt_req_ack(dest, pEvent, NULL);
                }

                const struct sockaddr_in *r_addr = (struct sockaddr_in *)recv_addr;
                const in_addr_t *addr = (in_addr_t *)&r_addr->sin_addr;
                const uint16_t port = (uint16_t)r_addr->sin_port;
                mcu_smcp10_updatePeerAddressCache(addr, port);

                // Append the event in target executor queue
                retval = mcu_executorInsertEvent(dest, pEvent);
            }
        }
        else
            retval = MCU_ERROR_NO_ACCESS;

        // The last step. Release the allocated resources.
        mcuFree(pEvent);
    }
    return retval;
}

mcu_error mcu_smcp10_read_param(uint32_t paramId, int *value, bool refresh_cache)
{
    mcu_error retval = MCU_ERROR_OVERFLOW;

    if (paramId < SMCP10_PARAM_COUNT)
    {
        retval = MCU_ERROR_NONE;

        if (refresh_cache)
        {
            // TODO
        }

        if (retval == MCU_ERROR_NONE)
            *value = smcp10_params_cache[paramId];
    }
    else if (paramId >= SMCP10_PARAM_DYNAMIC_START &&
             paramId <= SMCP10_PARAM_DYNAMIC_END)
    {
        retval = MCU_ERROR_NONE;
        switch (paramId)
        {
        default:
            retval = MCU_ERROR_NOT_FOUND;
            break;
        }
    }
    else if (paramId > SMCP10_PARAM_READONLY_START &&
             paramId <= SMCP10_PARAM_READONLY_END)
    {

        double angle;
        retval = MCU_ERROR_NONE;
        switch (paramId)
        {
        case SMCP1_PARAM_HW_ID:
        case SMCP1_PARAM_SN:
        case SMCP1_PARAM_EOW:
        {
            const hwInfo *pHwInfo = mcu_commonGetHwInfo(false);
            if (paramId == SMCP1_PARAM_HW_ID)
                *value = (int)pHwInfo->hwid;
            else if (paramId == SMCP1_PARAM_SN)
                *value = (int)pHwInfo->serialNbr;
            else if (paramId == SMCP1_PARAM_EOW)
                *value = (int)pHwInfo->manufactured;
            else
                retval = MCU_ERROR_PARAMETER;
            break;
        }
        case SMCP1_PARAM_AXIS_COUNT:
        {
            systemInfo *pSystemInfo = mcu_commonGetSystemConf(false);
            int tmpValue = (int)mcu_commonResolveActuatorAxisCount(pSystemInfo);
            *value = tmpValue;
            break;
        }
        default:
            retval = MCU_ERROR_OVERFLOW;
            break;
        }
    }
    return retval;
}

mcu_error mcu_smcp10_write_param(uint32_t paramID, int value, bool write_through)
{
    mcu_error retval = MCU_ERROR_NO_ACCESS;
    if (paramID < SMCP10_PARAM_COUNT)
    {
        retval = MCU_ERROR_NONE;
        if (write_through)
        {
            // TODO
        }
        if (retval == MCU_ERROR_NONE)
        {
            smcp10_params_cache[paramID] = value;
        }
    }
    return retval;
}

uint32_t mcu_commonResolveActuatorAxisCount(const systemInfo *info)
{
    uint32_t retVal = 0;
    const systemInfo *sysInfoPtr = info ? info : mcu_commonGetSystemConf(false);
    for (int act = ACTUATOR_FIRST; act <= ACTUATOR_LAST; act++)
    {
        if (mcu_commonIsActuatorEnabled(act, sysInfoPtr))
        {
            retVal++;
        }
    }
    return retVal;
}

static mcu_error mcu_smcp10_set_default_params()
{
    int param;
    int value;

    mcu_error retval;

    for (param = 0; param < SMCP10_PARAM_COUNT; param++)
    {

        retval = mcu_smcp10_read_param(param, &value, false);

        if (retval == MCU_ERROR_NONE)
        {

            if (value == SMCP10_PARAM_UNINITIALIZED)
            {

                switch (param)
                {
                case SMCP1_PARAM_MEM_SPEED:
                    value = 4;
                    break;
                case SMCP1_PARAM_DEV_ID:
                {
                    int tmcDevId = integrationGetDev();
                    if (tmcDevId == 0)
                    {
                        value = 1;
                        integrationSetDev(value);
                    }
                    else
                    {
                        value = tmcDevId;
                    }
                    break;
                }
                case SMCP1_PARAM_AXIS_DRIVE_ORDER:
                    value = ACTUATOR_0;
                    value |= (ACTUATOR_2 << 8);
                    value |= (ACTUATOR_1 << 16);
                    value |= (ACTUATOR_3 << 24);
                    break;
                default:
                    break;
                }

                // Update the cache when needed
                if (value != SMCP10_PARAM_UNINITIALIZED)
                    retval = mcu_smcp10_write_param(param, value, false);
            }
        }

        if (retval != MCU_ERROR_NONE)
        {
            break;
        }
    }
    return retval;
}

static mcu_error smcp10_set_param_refresh_system(uint32_t paramId, int value)
{
    mcu_error retval = MCU_ERROR_NONE;

    switch (paramId)
    {
    case SMCP1_PARAM_UDP_PORT:
    {
        systemInfo *pSysInfo = mcu_commonGetSystemConf(false);
        pSysInfo->udp_port = (value & 0xffff);
        retval = mcu_commonSetSystemConf(pSysInfo, true);
        if (retval == MCU_ERROR_NONE)
        {
            // TODO re-init socket connection with new port
        }
        break;
    }
    case SMCP1_PARAM_DEV_ID:
        integrationSetDev(value);
        break;

    default:
        break;
    }
    return retval;
}

static mcu_error mcu_smcp10_read_mem_drive_target(mcu_actuator act, uint32_t memIndex, int32_t *destPos)
{
    return integrationGetMemDriveTarget(act, memIndex, destPos);
}

static mcu_error smcp10_resolve_mem_drive_order(uint32_t mem_index, bool *resultReverse)
{
    uint32_t driveOrder;
    mcu_error retval = mcu_smcp10_read_param(SMCP1_PARAM_AXIS_DRIVE_ORDER,
                                             (int *)&driveOrder, false);

    if (retval == MCU_ERROR_NONE)
    {
        systemInfo *info = mcu_commonGetSystemConf(false);
        if (mcu_commonResolveActuatorAxisCount(info) > 0)
        {
            uint32_t i;
            uint8_t *pAct = (uint8_t *)&driveOrder;
            bool found = false;

            for (i = 0; i < sizeof driveOrder; i++)
            {
                if (mcu_commonIsActuatorEnabled(*pAct, info))
                {
                    found = true;
                    break;
                }
                pAct++;
            }

            if (found)
            {
                // Resolve current and target positions
                int currentPos;
                int32_t targetPos;
                uint8_t tmp = *pAct;
                mcu_actuator act = (mcu_actuator)tmp;
                retval = mcu_actuatorGetPosition(act, &currentPos, false);
                if (retval == MCU_ERROR_NONE)
                {
                    retval = mcu_smcp10_read_mem_drive_target(act, mem_index, &targetPos);
                    // Set the actual result
                    if (retval == MCU_ERROR_NONE)
                    {
                        bool reverse = true;
                        if (currentPos > (int)targetPos)
                        {
                            reverse = false;
                        }

                        *resultReverse = reverse;
                    }
                }
            }
        }
    }

    return retval;
}

/**************** The SCMP1 command handlers ****************/

int handle_smcp10_cmd_get_parameter(mcu_event_queue target, mcu_event *pEvent)
{
    UNUSED(target);
    char *pSb_data;
    smcp1_sb_header *pSb;

    mcu_error retval = mcu_smcpv1_get_sub_block_data(pEvent, 0, &pSb, &pSb_data);

    if (retval == MCU_ERROR_NONE)
    {

        retval = MCU_ERROR_PARAMETER;

        if (pSb->size >= 1)
        {

            uint32_t paramId = *(uint32_t *)smcpv10_get_data(pSb, pSb_data, 0);
            int value = -1;

            retval = mcu_smcp10_read_param(paramId, &value, false);
            if (retval == MCU_ERROR_NONE)
            {

                int32_t replyData[2];
                replyData[0] = (int32_t)paramId;
                replyData[1] = value;

                char *reply = smcpv10_compose_reply(pEvent, (char *)&replyData[0], 2, SMCP1_DATA_INT32);

                if (reply)
                    pEvent->pData1 = reply;
                else
                    retval = MCU_ERROR_NO_MEMORY;
            }
        }
        //   MCUTRACE(("%s param = %d, value = %d\n", __func__, (int)paramId, value));
    }
    return retval;
}

int handle_smcp10_cmd_set_parameter(const mcu_event_queue target, mcu_event *pEvent)
{
    UNUSED(target);
    mcu_error retval;

    char *pSb_data;
    smcp1_sb_header *pSb;

    retval = mcu_smcpv1_get_sub_block_data(pEvent, 0, &pSb, &pSb_data);
    if (retval == MCU_ERROR_NONE)
    {

        if (pSb->size >= 2)
        {

            uint32_t paramId = *(uint32_t *)smcpv10_get_data(pSb, pSb_data, 0);
            int value = *(int *)smcpv10_get_data(pSb, pSb_data, 1);

            // Write normal static data in EEPROM
            if (paramId < SMCP10_PARAM_DYNAMIC_START)
            {
                retval = mcu_smcp10_write_param(paramId, value, true);
            }

            if (retval == MCU_ERROR_NONE)
            {
                // Refresh
                retval = smcp10_set_param_refresh_system(paramId, value);
            }
            MCUTRACE(("%s param = %d, value = %d\n", __func__, (int)paramId, value));
        }
        else
        {
            retval = MCU_ERROR_PARAMETER;
        }
    }
    return retval;
}

int handle_smcp10_cmd_get_version(mcu_event_queue target, mcu_event *pEvent)
{
    UNUSED(target);
    mcu_error retval = MCU_ERROR_NONE;

    uint32_t replyData[4];
    replyData[0] = SMCP_PROXY_VERSION_MAJOR;
    replyData[1] = SMCP_PROXY_VERSION_YEAR;
    replyData[2] = SMCP_PROXY_VERSION_WEEK;
    replyData[3] = SMCP_PROXY_VERSION_DAY_AND_BUILD;

    char *reply = smcpv10_compose_reply(pEvent, (char *)&replyData[0], 4, SMCP1_DATA_UINT32);

    if (reply)
        pEvent->pData1 = reply;
    else
        retval = MCU_ERROR_NO_MEMORY;

    return retval;
}

int handle_smcp10_cmd_get_info_text(mcu_event_queue target, mcu_event *pEvent)
{
    UNUSED(target);

    char buffer[120];
    char *pCtrlFirmwareVersion = integrationGetFirmwareVersion();
    sprintf(buffer, "Primary: SPXY v%d.%02d.%02d.%03d\nSecondary: %s", SMCP_PROXY_VERSION_MAJOR,
            SMCP_PROXY_VERSION_YEAR, SMCP_PROXY_VERSION_WEEK, SMCP_PROXY_VERSION_DAY_AND_BUILD,
            pCtrlFirmwareVersion);

    mcu_error retval = MCU_ERROR_NONE;

    int payloadSize = (int)strlen(buffer);

    char *reply = smcpv10_compose_reply(pEvent, (char *)&buffer[0], payloadSize, SMCP1_DATA_CHAR_STRING);

    if (reply)
    {
        pEvent->pData1 = reply;
    }
    else
    {
        retval = MCU_ERROR_NO_MEMORY;
    }

    return retval;
}

int handle_smcp10_cmd_take_step(mcu_event_queue target, mcu_event *pEvent)
{
    char *pSbData;
    smcp1_sb_header *pSbHdr;

    // Resolve params
    mcu_error retval = mcu_smcpv1_get_sub_block_data(pEvent, 0, &pSbHdr, &pSbData);

    if (retval == MCU_ERROR_NONE)
    {

        if (pSbHdr->size >= 7)
        {
            systemInfo *info = mcu_commonGetSystemConf(false);

            // Distribute event to all relevant actuators
            if (target == MCU_EVENT_QUEUE_COMM)
            {

                for (int act = ACTUATOR_FIRST; act <= ACTUATOR_LAST; act++)
                {
                    // Append the event in target executor queue if data differs from zero
                    if (mcu_commonIsActuatorEnabled(act, info))
                    {
                        int32_t step_nm = *(int32_t *)smcpv10_get_data(pSbHdr, pSbData, (uint32_t)act);
                        int32_t speed_ums = *(int32_t *)smcpv10_get_data(pSbHdr, pSbData, (uint32_t)act + 4);
                        if (step_nm != 0 && speed_ums > 0)
                        {
                            retval = mcu_executorInsertEvent(act, pEvent);
                            if (retval != MCU_ERROR_NONE)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                // Get step len and speed
                int32_t step_nm = *(int32_t *)smcpv10_get_data(pSbHdr, pSbData, (uint32_t)target);
                int32_t speed_ums = *(int32_t *)smcpv10_get_data(pSbHdr, pSbData, (uint32_t)target + 4);

                /*
                 * // TODO limit checks
                bool limitCheckEnabled = smcp_common_IsFeatureEnabled(
                        SMCP10_FEAT_TRANSITION_LIMITS, &info->features, &info->features_mask);

                if (limitCheckEnabled) {
                    int actPos;
                    retval = mcu_actuatorGetPosition((mcu_actuator)target, &actPos);
                    if (retval == MCU_ERROR_NONE) {
                        retval = mcu_actuatorIsWithinTransitionLimits((mcu_actuator)target,
                                                                      actPos, direction);
                    }
                }
                */
                if (retval == MCU_ERROR_NONE)
                {
                    retval = integrationTakeStep((mcu_actuator)target, speed_ums, step_nm);
                }
            }
        }
        else
        {
            retval = MCU_ERROR_UNDERFLOW;
        }
    }
    else
    {
        retval = MCU_ERROR_PARAMETER;
    }

    return retval;
}

int handle_smcp10_cmd_take_legacy_step(mcu_event_queue target, mcu_event *pEvent)
{
    char *pSbData;
    smcp1_sb_header *pSbHdr;

    // Resolve params
    mcu_error retVal = mcu_smcpv1_get_sub_block_data(pEvent, 0, &pSbHdr, &pSbData);

    if (retVal == MCU_ERROR_NONE)
    {
        if (pSbHdr->size >= 2)
        {
            int32_t *pSpeed = (int32_t *)smcpv10_get_data(pSbHdr, pSbData, 1);

            if (target == MCU_EVENT_QUEUE_COMM)
            {
                systemInfo *pSysInfo = mcu_commonGetSystemConf(false);

                int lastAct = pSbHdr->size - 1;
                if (lastAct > ACTUATOR_LAST)
                {
                    lastAct = ACTUATOR_LAST;
                }

                for (int act = ACTUATOR_FIRST; act <= lastAct; act++)
                {
                    if (*pSpeed)
                    {
                        if (mcu_commonIsActuatorEnabled(act, pSysInfo))
                        {
                            mcu_error status = mcu_executorInsertEvent((mcu_event_queue)act, pEvent);
                            if (status != MCU_ERROR_NONE)
                            {
                                retVal = status;
                            }
                        }
                    }
                    pSpeed++;
                }
            }
            else
            {
                pSpeed += target;
                if (*pSpeed)
                {
                    int32_t gear = *(int32_t *)smcpv10_get_data(pSbHdr, pSbData, 0);
                    retVal = integrationTakeLegacyStep((mcu_actuator)target, gear, *pSpeed);
                }
            }
        }
        else
        {
            retVal = MCU_ERROR_PARAMETER;
        }
    }

    return retVal;
}

int handle_smcp10_cmd_store_mem(const mcu_event_queue target, mcu_event *pEvent)
{
    UNUSED(target);
    mcu_error retval;
    char *pSbData;
    smcp1_sb_header *pSb;

    // Resolve params
    retval = mcu_smcpv1_get_sub_block_data(pEvent, 0, &pSb, &pSbData);
    if (retval == MCU_ERROR_NONE)
    {

        if (pSb->size >= 1)
        {
            int act;
            int32_t position;

            uint32_t index = *(uint32_t *)smcpv10_get_data(pSb, pSbData, 0);

            systemInfo *info = mcu_commonGetSystemConf(false);

            for (act = ACTUATOR_FIRST; act <= ACTUATOR_LAST; act++)
            {

                if (mcu_commonIsActuatorEnabled(act, info))
                {
                    // Read the current position of an actuator ...
                    retval = mcu_actuatorGetPosition(act, (int *)&position, true);
                    if (retval == MCU_ERROR_NONE)
                    {
                        // ... and store it.
                        retval = integrationSetMemDriveTarget((mcu_actuator)act, index, position);
                    }

                    if (retval != MCU_ERROR_NONE)
                    {
                        break;
                    }
                }
            }
        }
        else
        {
            retval = MCU_ERROR_PARAMETER;
        }
    }
    MCUTRACE(("%s (%d)\n", __func__, retval));
    return retval;
}

int handle_smcp10_cmd_goto_mem(const mcu_event_queue target, mcu_event *pEvent)
{
    UNUSED(target);
    bool reverseOrder = false;
    int retval = MCU_ERROR_NONE;

    // Resolve drive order
    if (target == MCU_EVENT_QUEUE_COMM)
    {
        char *pSbData;
        smcp1_sb_header *pSb;
        retval = mcu_smcpv1_get_sub_block_data(pEvent, 0, &pSb, &pSbData);
        if (retval == MCU_ERROR_NONE)
        {
            if (pSb->size >= 1)
            {
                uint32_t mem_index = *(uint32_t *)smcpv10_get_data(pSb, pSbData, 0);
                retval = smcp10_resolve_mem_drive_order(mem_index, &reverseOrder);
            }
            else
            {
                retval = MCU_ERROR_PARAMETER;
            }
        }
    }

    if (retval == MCU_ERROR_NONE)
    {
        retval = do_handle_smcp10_cmd_goto_mem(target, pEvent, reverseOrder);
    }

    return retval;
}

static uint32_t mcu_swap32(uint32_t n)
{
    return __builtin_bswap32(n);
}

static mcu_error smcp10_forward_next_target(const mcu_event_queue current,
                                            mcu_event *pEvent, bool reverseOrder, uint32_t mask)
{
    uint32_t driveOrder;
    mcu_error retval = mcu_smcp10_read_param(SMCP1_PARAM_AXIS_DRIVE_ORDER,
                                             (int *)&driveOrder, false);

    if (reverseOrder)
    {
        uint32_t reverse = mcu_swap32(driveOrder);
        driveOrder = reverse;
    }

    if (retval == MCU_ERROR_NONE)
    {
        uint32_t i;
        systemInfo *info = mcu_commonGetSystemConf(false);
        uint8_t *pAct = (uint8_t *)&driveOrder;

        retval = MCU_ERROR_NOT_FOUND;

        if (current == MCU_EVENT_QUEUE_COMM)
        {
            for (i = 0; i < sizeof driveOrder; i++)
            {
                if (mcu_commonIsActuatorEnabled(*pAct, info) && (mask & (1 << *pAct)))
                {
                    retval = mcu_executorInsertEvent((mcu_event_queue)*pAct, pEvent);
                    break;
                }
                pAct++;
            }
        }
        else
        {
            // Forward the event to the next actuator
            bool foundmyself = false;

            for (i = 0; i < sizeof driveOrder; i++)
            {
                if (!foundmyself && (mcu_event_queue)*pAct == current)
                {
                    foundmyself = true; // Next is the one we need
                    pAct++;
                    continue;
                }
                // Search the next target actuator
                if (foundmyself)
                {
                    if (mcu_commonIsActuatorEnabled(*pAct, info) && (mask & (1 << *pAct)))
                    {
                        retval = mcu_executorInsertEvent((mcu_event_queue)*pAct, pEvent);
                        break;
                    }
                }
                pAct++;
            }
        }
    }
    return retval;
}

int handle_smcp10_cmd_goto_mem_reverse(const mcu_event_queue target, mcu_event *pEvent)
{
    return do_handle_smcp10_cmd_goto_mem(target, pEvent, true);
}

#define SMCP10_MASK_ALL_ACTUATORS 0b1111
static int do_handle_smcp10_cmd_goto_mem(const mcu_event_queue target, mcu_event *pEvent, bool reverseOrder)
{
    char *pSbData;
    smcp1_sb_header *pSb;

    // Resolve params
    mcu_error retval = mcu_smcpv1_get_sub_block_data(pEvent, 0, &pSb, &pSbData);

    if (retval == MCU_ERROR_NONE)
    {

        bool simultant_drive = false;
        if (pSb->size >= 3)
        {
            uint32_t axis_drive_mode = *(uint32_t *)smcpv10_get_data(pSb, pSbData, 2);
            if (axis_drive_mode == 1)
            {
                simultant_drive = true;
            }
        }
        uint32_t targetMask = SMCP10_MASK_ALL_ACTUATORS;
        if (pSb->size >= 5)
        {
            targetMask = *(uint32_t *)smcpv10_get_data(pSb, pSbData, 4);
        }

        if (target == MCU_EVENT_QUEUE_COMM)
        {

            // Set the command type based on the drive order
            if (reverseOrder)
            {
                pEvent->frame.type = SMCP1_CMD_GOTO_MEM_REVERSE;
            }
            else
            {
                pEvent->frame.type = SMCP1_CMD_GOTO_MEM;
            }

            if (!simultant_drive)
            {
                // ... forward the modified message to the first actuator.
                retval = smcp10_forward_next_target(target, pEvent, reverseOrder, targetMask);
            }
            else
            {
                // ... forward the modified message to all actuators simultaneously
                mcu_actuator act;
                systemInfo *info = mcu_commonGetSystemConf(false);

                // Forward the message to all executors at the same time.
                for (act = ACTUATOR_FIRST; act <= ACTUATOR_LAST; act++)
                {
                    if (mcu_commonIsActuatorEnabled(act, info))
                    {
                        // ... append
                        retval = mcu_executorInsertEvent(act, pEvent);

                        if (retval != MCU_ERROR_NONE)
                        {
                            break;
                        }
                    }
                }
            }
        }
        else
        {

            if (pSb->size >= 2)
            {

                int targetPos;
                uint32_t memIndex = *(uint32_t *)smcpv10_get_data(pSb, pSbData, 0);
                int speed = *(int *)smcpv10_get_data(pSb, pSbData, 1);

                retval = mcu_smcp10_read_mem_drive_target((mcu_actuator)target, memIndex, (int32_t *)&targetPos);

                if (retval == MCU_ERROR_NONE)
                {
                    // Finally. Process the operation
                    retval = integrationGotoPosition((mcu_actuator)target, targetPos, speed, simultant_drive);

                    if (!simultant_drive)
                    {
                        // Forward the event to next target
                        if (retval == MCU_ERROR_NONE)
                        {
                            mcu_error status = smcp10_forward_next_target(target, pEvent, reverseOrder, targetMask);
                            if (status != MCU_ERROR_NOT_FOUND)
                            {
                                retval = status;
                            }
                        }
                    }
                }
            }
            else
            {
                retval = MCU_ERROR_PARAMETER;
            }
        }
    }
    return retval;
}

int handle_smcp10_cmd_stop(const mcu_event_queue target, mcu_event *pEvent)
{
    UNUSED(target);
    UNUSED(pEvent);

    mcu_error retVal = MCU_ERROR_NONE;
    systemInfo *info = mcu_commonGetSystemConf(false);

    for (mcu_actuator act = ACTUATOR_FIRST; act <= ACTUATOR_LAST; act++)
    {
        if (mcu_commonIsActuatorEnabled(act, info))
        {
            // Flush actuator event queues
            mcu_error status = mcu_executorRemoveAll(act);
            if (status == MCU_ERROR_NONE)
            {
                // Stop the move
                status = integrationStop(act);
                if (status != MCU_ERROR_NONE)
                {
                    retVal = status;
                }
            }
        }
    }

    MCUTRACE(("%s, retval = %d\n", __func__, retVal));
    return retVal;
}
