/*-----------------------------------------------------------------------------
 *  File: port.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef PORT_H
#define PORT_H
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "constants.h"
#include "functions.h"

#define osOK            0
#define osWaitForever   (-1)

//#define DEBUG_SMCPV1_PROXY_MALLOC
#ifdef DEBUG_SMCPV1_PROXY_MALLOC
void *mcuMalloc(size_t size);
void mcuFree(void *ptr);
#else
#define mcuMalloc       malloc
#define mcuFree         free
#endif

//#define DEBUG_SMCPV1_PROXY_TRACE_ENABLED
#ifdef DEBUG_SMCPV1_PROXY_TRACE_ENABLED
// TODO Implement trace
#define MCUTRACE(...) printf("%s  ", timeString()); printf __VA_ARGS__
#else
#define MCUTRACE(x)
#endif

// OS specific
typedef pthread_mutex_t osMutexId;
typedef pthread_t osThreadId;
typedef int osStatus;

osStatus osMutexWait(osMutexId* mutex, int timeoutMs);
osStatus osMutexRelease(osMutexId* mutex);
osThreadId osThreadGetId(void);
osStatus osThreadYield (void);
osStatus osThreadSuspend (osThreadId thread_id);
osStatus osThreadResume (osThreadId thread_id);

// SOC Vendor specific
uint32_t HAL_GetTick();

#endif // PORT_H
