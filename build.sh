# Remove the executeable folder if it exists to force regeneration
[ -d "./executeable" ] && rm -rf ./executeable
# Create the executeable folder
mkdir ./executeable
# Build the executeable file from the buildtree into build folder
sudo cmake --build ./build --target smcp1_proxy
# Move the executeable file into the executeable folder
sudo mv ./build/smcp1_proxy ./executeable/smcp1_proxy
