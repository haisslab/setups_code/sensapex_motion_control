/*-----------------------------------------------------------------------------
 *  File: executor.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "constants.h"
#include "smcp1.h"
#define MCU_EXEC_MUTEX_WAIT_MS osWaitForever
#define MCU_EVENT_OPTIONS_MAX_COUNT 32

#define MCU_EXECUTOR_EVENT_VIRTUAL_AXIS_IND_BIT (1 << 28)
#define MCU_EXECUTOR_EVENT_STEP_SCALER_IND_BIT (1 << 29)
#define MCU_EXECUTOR_EVENT_QUEUE_3_IND_BIT (1 << 30)

#define MCU_EXECUTOR_EVENT_LOCKS_CNT 5

typedef struct
{
    volatile uint32_t locks[MCU_EXECUTOR_EVENT_LOCKS_CNT];
    volatile uint32_t error;
    volatile char *pDataDeferred;
} mcu_event_status;

typedef enum
{
    // Basic data types 0 - 9
    EVENT_SB_TYPE_VOID = 0,
    EVENT_SB_TYPE_UINT8 = 1,
    EVENT_SB_TYPE_INT8 = 2,
    EVENT_SB_TYPE_UINT16 = 3,
    EVENT_SB_TYPE_INT16 = 4,
    EVENT_SB_TYPE_UINT32 = 5,
    EVENT_SB_TYPE_INT32 = 6,
    EVENT_SB_TYPE_UINT64 = 7,
    EVENT_SB_TYPE_INT64 = 8,

    // would there be any use for packed data types?
    EVENT_SB_TYPE_UINT8_ARRAY_2 = 10,
    EVENT_SB_TYPE_INT8_ARRAY_2 = 11,
    EVENT_SB_TYPE_UINT8_ARRAY_3 = 12,
    EVENT_SB_TYPE_INT8_ARRAY_3 = 13,
    EVENT_SB_TYPE_UINT8_ARRAY_4 = 14,
    EVENT_SB_TYPE_INT8_ARRAY_4 = 15,

    EVENT_SB_TYPE_UINT16_ARRAY_2 = 20,
    EVENT_SB_TYPE_INT16_ARRAY_2 = 21,
} mcu_event_sb_type;

typedef struct
{
    // 0 (byte index in the receive buffer)
    uint8_t version; // Protocol version, for smcpv1 == 0x10, smcpv0 version was 0x02
    // 1
    uint8_t sender_type; // Device type of message sender (smcp1_device_category)
    // 2-3
    uint16_t receiver_id; // ID of message receiver (1, 2, 3 ... , 255 to multicast to all manipulators with id < 255)
    // 4-5
    uint16_t sender_id; // ID of message sender (CUBa's Id > 0x100, PC's Id > 0x200, values above 0x1000 reserved for future)
    // 6-7
    uint16_t message_id; // Message ID (autoincremented counter)
    // 8-11
    uint32_t options; // Bitmask i.e. message is a response or request an ACK when operation is completed
    // 12-13
    uint16_t type; // Message type/command
    // 14-15
    uint16_t sub_blocks; // The total count of the following sub block(s), zero for command without arguments
    // 0-N sub blocks to follow on wire
} smcp1_exec_frame;

typedef struct
{
    // These fields are MCU specific fields
    uint16_t priority;            // Priority of task
    struct sockaddr_in replyaddr; // IP address for UDP reply
    char *pData1;                 // Internal databuffer 1
    mcu_event_status *pStatus;    // Event status info
    uint16_t rootType;            // The message type/command of root event.
    uint16_t extra2;              // Not used currently. Defined just for data alignment reasons.
    uint32_t extra3;              // Not used currently. Defined just for data alignment reasons.
    smcp1_exec_frame frame;       // Keep this as the very last element. It essential to have this aligned.
} mcu_event;

typedef struct
{
    uint16_t type;
    uint16_t size;
} mcu_event_sb_header;

typedef enum
{
    MCU_EVENT_PRIORITY_IGNORED = 0, // This is used to mark handled events
    MCU_EVENT_PRIORITY_HIGH = 1,
    MCU_EVENT_PRIORITY_NORMAL = 127,
    MCU_EVENT_PRIORITY_LOW = 255
} mcu_event_priority;

typedef enum
{
    MCU_EVENT_QUEUE_0 = ACTUATOR_0,
    MCU_EVENT_QUEUE_1 = ACTUATOR_1,
    MCU_EVENT_QUEUE_2 = ACTUATOR_2,
    MCU_EVENT_QUEUE_3 = ACTUATOR_3,
    MCU_EVENT_QUEUE_COMM = MCU_EVENT_QUEUE_3 + 20
} mcu_event_queue;

typedef void (*pre_exec_handler)(const mcu_event_queue, mcu_event *, mcu_event_status *);
typedef int (*cmd_exec_handler)(const mcu_event_queue, mcu_event *);
typedef void (*error_handler)(mcu_event *, int);
typedef void (*post_exec_handler)(const mcu_event_queue, mcu_event *, mcu_event_status *);

typedef cmd_exec_handler (*cmd_resolver)(int);

typedef struct
{
    pre_exec_handler *preCmdHandlers; // Pre-execution options function pointer table
    cmd_resolver cmdResolver;
    error_handler execErrorHandler;
    post_exec_handler *postCmdHandlers; // Post-execution options function pointer table
} execHandlerInfo;

mcu_error mcu_executorInit();
mcu_error mcu_executorInsertEvent(mcu_event_queue target, const mcu_event *event);
mcu_error mcu_executorPopEvent(mcu_event_queue target, mcu_event **pEvent);

_Noreturn void mcu_executorStart(mcu_event_queue target);
mcu_error mcu_executorDelegateEvent(mcu_event *target, mcu_event *source);
mcu_error mcu_executorRemoveAll(mcu_event_queue target);
mcu_error mcu_executorSizeOfEvent(const mcu_event *event, size_t *size);

bool mcu_executorIsEventDone(mcu_event_status *pStatus);
mcu_error mcu_executorStatusInsertPayload(mcu_event_status *pStatus, char *pData);

#endif
