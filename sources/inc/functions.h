/*-----------------------------------------------------------------------------
 *  File: functions.h
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2023-2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdint.h>
#include <stdbool.h>
#include "constants.h"
#include "smcp1.h"
#include "tmcl.h"

extern int serial_fd;

void proxyInit(unsigned int udp_port, char *serial_device, const char *udp_address);
void proxyDeinit();

void *readUdp(void *arg);

uint64_t ntohll_custom(uint64_t netlonglong);
uint64_t htonll_custom(uint64_t hostlonglong);

void sendSMCP1Heartbeat();
void sendSMCP1PositionChangedNotification(bool multiUnicast);
mcu_error mcu_smcp10BroadcastStatusChangedNotification(uint32_t status);

char *timeString();

mcu_error sendUdpData(const uint8_t *aData, int dataSize, const struct sockaddr *addr);
mcu_error sendUdpMessage(const smcp1_frame *aSmcp1Msg, const struct sockaddr *addr);
mcu_error sendTcmlRequest(const tmclReq *aReq, tmclResp *aResp);

#endif // FUNCTIONS_H