/*-----------------------------------------------------------------------------
 *  File: integration.c
 *
 *  This file is part of Sensapex smcp1_proxy codebase.
 *
 *  Created by Veli-Matti Kananen
 *  Copyright (c) 2024 Sensapex Oy. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "integration.h"
#include "functions.h"
#include "port.h"
#include "misc.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>

static int32_t integrationGetLegacySpeed(int gear);
static int32_t integrationGetLegacyStepLength(int gear);
static mcu_error integrationsDoTakeStep(mcu_actuator act, int32_t speed, int32_t target, bool absoluteTarget, bool waitFinish);

static int32_t gAxisMemTarget[MCU_SYSTEM_ACT_MAX_COUNT][2] = {0};
static uint32_t gAxisSpeed[MCU_SYSTEM_ACT_MAX_COUNT] = {0};
static uint16_t gIntegrationDeviceId = 0;

mcu_error integrationInitActuator(mcu_actuator act)
{
    tmclReq tmclRequest;
    tmclResp tmclResponse = {0};

    // Read current position
    tmclRequest.addr = integrationGetDev();
    tmclRequest.moduleAddr = act;
    tmclRequest.cmd = tmclCmdGetAxisParameter;
    tmclRequest.type = tmclAxisParamActualPosition;
    tmclRequest.data = 0;
    tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

    mcu_error retVal = sendTcmlRequest(&tmclRequest, &tmclResponse);
    if (retVal == MCU_ERROR_NONE)
    {
        // Update the position
        tmclSetPosition(act, (int32_t)tmclResponse.data);
    }
    else
    {
        retVal = MCU_ERROR_NOT_INITIALIZED;
    }

    // Set max positioning speed
    if (retVal == MCU_ERROR_NONE)
    {
        tmclRequest.moduleAddr = act;
        tmclRequest.cmd = tmclCmdSetAxisParameter;
        tmclRequest.type = tmclAxisParamMaxPositioningSpeed;
        tmclRequest.data = TMCL_AXIS_PARAM_MAX_POSITIONING_SPEED;
        tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

        retVal = sendTcmlRequest(&tmclRequest, &tmclResponse);
    }
    return retVal;
}

uint16_t integrationGetDev()
{
    if (gIntegrationDeviceId == 0)
    {
        // Fetch from the device
        tmclResp tmclResponse;

        tmclReq tmclRequest;
        tmclRequest.cmd = tmclCmdGetGlobalParameter;
        tmclRequest.addr = 0; // Serial address
        tmclRequest.moduleAddr = 0;
        tmclRequest.type = tmclGlobalParamSerialAddress;
        tmclRequest.data = 0;
        tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

        mcu_error error = sendTcmlRequest(&tmclRequest, &tmclResponse);
        if (error == MCU_ERROR_NONE)
        {
            gIntegrationDeviceId = tmclResponse.data;
        }
    }

    return gIntegrationDeviceId;
}

uint16_t integrationSetDev(uint16_t devId)
{
    if (devId != gIntegrationDeviceId && devId < 255)
    {
        // Fetch from the device
        tmclResp tmclResponse;

        tmclReq tmclRequest;
        tmclRequest.cmd = tmclCmdSetGlobalParameter;
        tmclRequest.addr = 0; // Serial address
        tmclRequest.moduleAddr = 0;
        tmclRequest.type = tmclGlobalParamSerialAddress;
        tmclRequest.data = devId;
        tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

        mcu_error error = sendTcmlRequest(&tmclRequest, &tmclResponse);
        if (error == MCU_ERROR_NONE)
        {
            gIntegrationDeviceId = tmclResponse.data;
        }
    }
    gIntegrationDeviceId = devId;
    return gIntegrationDeviceId;
}

char *integrationGetFirmwareVersion()
{
    static char firmwareVersion[100] = {'\0'};
    if (strlen(firmwareVersion) == 0)
    {
        // Fetch from the device
        tmclResp tmclResponse;

        tmclReq tmclRequest;
        tmclRequest.cmd = tmclCustomGetFirmwareVersion;
        tmclRequest.addr = integrationGetDev();
        tmclRequest.moduleAddr = 0;
        tmclRequest.type = 1;
        tmclRequest.data = 0;
        tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

        mcu_error error = sendTcmlRequest(&tmclRequest, &tmclResponse);
        if (error == MCU_ERROR_NONE)
        {
            uint16_t model = tmclResponse.data >> 16;
            uint8_t versionMajor = (tmclResponse.data >> 8) & 0xff;
            uint8_t versionMinor = tmclResponse.data & 0xff;

            sprintf(firmwareVersion, "%d v%d.%d", model, versionMajor, versionMinor);
        }
    }
    return firmwareVersion;
}

mcu_error integrationTakeStep(mcu_actuator act, int32_t speed_ums, int32_t step_nm)
{
    return integrationsDoTakeStep(act, speed_ums * SMCP_TO_TMCL_SPEED_MULTIPLIER, (step_nm / SMCP_TO_TMCL_STEP_MULTIPLIER + 1), false, false);
}

mcu_error integrationTakeLegacyStep(mcu_actuator act, int32_t gear, int32_t speed)
{
    int32_t tmcSpeed = integrationGetLegacySpeed(gear);
    int32_t stepTicks = integrationGetLegacyStepLength(gear);
    if (speed < 0)
    {
        stepTicks = -stepTicks;
    }
    mcu_error retVal = integrationsDoTakeStep(act, tmcSpeed, stepTicks, false, false);
    return retVal;
}

mcu_error integrationSetMemDriveTarget(mcu_actuator act, uint32_t memIndex, int32_t position)
{

    mcu_error retVal = MCU_ERROR_NONE;
    memIndex--;
    if (memIndex < 2)
    {
        gAxisMemTarget[act][memIndex] = position;
    }
    else
    {
        retVal = MCU_ERROR_PARAMETER;
    }
    return retVal;
}

mcu_error integrationGetMemDriveTarget(mcu_actuator act, uint32_t memIndex, int32_t *position)
{
    mcu_error retVal = MCU_ERROR_NONE;
    memIndex--;
    if (memIndex < 2)
    {
        *position = gAxisMemTarget[act][memIndex];
    }
    else
    {
        retVal = MCU_ERROR_PARAMETER;
    }
    return retVal;
}

mcu_error integrationGotoPosition(mcu_actuator act, int32_t position, uint32_t speed, bool async)
{
    return integrationsDoTakeStep(act, (int32_t)speed, position, true, true);
}

mcu_error integrationStop(mcu_actuator act)
{
    tmclReq tmclRequest;
    tmclResp tmclResponse;

    // Stop the actuator
    tmclRequest.addr = integrationGetDev();
    tmclRequest.moduleAddr = act;
    tmclRequest.cmd = tmclCmdStop;
    tmclRequest.type = 0;
    tmclRequest.data = 0;
    tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

    mcu_error retVal = sendTcmlRequest(&tmclRequest, &tmclResponse);
    return retVal;
}

/********************* FUNCTIONS ***********************/
mcu_error integrationGetPosition(mcu_actuator act, int *position, bool rawData)
{
    *position = (int)tmclGetPosition(act, rawData);
    return MCU_ERROR_NONE;
}

int32_t integrationGetLegacySpeed(int gear)
{
    int32_t speed = 0;
    switch (gear)
    {
    case 1:
        speed = 1;
        break;
    case 2:
        speed = 5;
        break;
    case 3:
        speed = 25;
        break;
    case 4:
        speed = 125;
        break;
    case 5:
        speed = 500;
        break;
    case 6:
        speed = 1500;
        break;
    case 7: // pen mode
        speed = 2000;
        break;
    default:
        assert(false);
    }
    return speed;
}

int32_t integrationGetLegacyStepLength(int gear)
{
    int32_t step = 0;
    switch (gear)
    {
    case 1:
        step = 1;
        break;
    case 2:
        step = 5;
        break;
    case 3:
        step = 25;
        break;
    case 4:
        step = 125;
        break;
    case 5:
        step = 500;
        break;
    case 6:
        step = 1000;
        break;
    case 7: // pen mode
        step = 500;
        break;
    default:
        assert(false);
    }
    return step;
}

mcu_error integrationsDoTakeStep(mcu_actuator act, int32_t speed, int32_t target, bool absoluteTarget, bool waitFinish)
{
    mcu_error retVal = MCU_ERROR_NONE;

    tmclReq tmclRequest;
    tmclResp tmclResponse;

    // Set the target speed
    if (speed != gAxisSpeed[act])
    {
        tmclRequest.addr = integrationGetDev();
        tmclRequest.moduleAddr = act;
        tmclRequest.cmd = tmclCmdSetAxisParameter;
        tmclRequest.type = tmclAxisParamMaxPositioningSpeed;
        tmclRequest.data = speed;
        tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

        retVal = sendTcmlRequest(&tmclRequest, &tmclResponse);
        if (retVal == MCU_ERROR_NONE)
        {
            gAxisSpeed[act] = speed;
        }
    }

    // Take the step
    if (retVal == MCU_ERROR_NONE)
    {
        tmclRequest.addr = integrationGetDev();
        tmclRequest.moduleAddr = act;
        tmclRequest.cmd = tmclCmdMoveToPosition;
        tmclRequest.type = absoluteTarget ? 0 : 1; // 0 == absolute, 1 == relative
        tmclRequest.data = target;
        tmclRequest.checkSum = tmclCalculateCheckSumReq(&tmclRequest);

        retVal = sendTcmlRequest(&tmclRequest, &tmclResponse);

        if (retVal == MCU_ERROR_NONE)
        {
            if (waitFinish)
            {
                mcu_actuatorSetBusyStatus(act, 1);
                // Wait if needed
                tmclReq tmclReqTargetReached;
                tmclReqTargetReached.addr = integrationGetDev();
                tmclReqTargetReached.moduleAddr = act;
                tmclReqTargetReached.cmd = tmclCmdGetAxisParameter;
                tmclReqTargetReached.type = tmclAxisParamTargetPosReached;
                tmclReqTargetReached.data = 0;
                tmclReqTargetReached.checkSum = tmclCalculateCheckSumReq(&tmclReqTargetReached);

                tmclReq tmclReqMotorStopped;
                tmclReqMotorStopped.addr = integrationGetDev();
                tmclReqMotorStopped.moduleAddr = act;
                tmclReqMotorStopped.cmd = tmclCmdGetAxisParameter;
                tmclReqMotorStopped.type = tmclAxisParamTargetSpeed;
                tmclReqMotorStopped.data = 0;
                tmclReqMotorStopped.checkSum = tmclCalculateCheckSumReq(&tmclReqMotorStopped);

                tmclReq tmclReqActualPosition;
                tmclReqActualPosition.addr = integrationGetDev();
                tmclReqActualPosition.moduleAddr = act;
                tmclReqActualPosition.cmd = tmclCmdGetAxisParameter;
                tmclReqActualPosition.type = tmclAxisParamActualPosition;
                tmclReqActualPosition.data = 0;
                tmclReqActualPosition.checkSum = tmclCalculateCheckSumReq(&tmclReqActualPosition);

                tmclResp tmclRespTargetReached = {0};
                tmclResp tmclRespMotorStopped = {0};
                tmclResp tmclRespActualPosition = {0};

                // Wait unit the motor gets
                int stallCounter = 0;
                do
                {
                    retVal = sendTcmlRequest(&tmclReqActualPosition, &tmclRespActualPosition);
                    tmclSetPosition(act, (int32_t)tmclRespActualPosition.data);

                    if (retVal == MCU_ERROR_NONE)
                    {
                        retVal = sendTcmlRequest(&tmclReqTargetReached, &tmclRespTargetReached);
                    }
                    if (retVal == MCU_ERROR_NONE && tmclRespTargetReached.data == 1)
                    {
                        break;
                    }
                    retVal = sendTcmlRequest(&tmclReqMotorStopped, &tmclRespMotorStopped);
                    if (retVal == MCU_ERROR_NONE)
                    {
                        if (tmclRespMotorStopped.data == 0)
                        {
                            stallCounter++;
                            if (stallCounter >= 10)
                            {
                                retVal = MCU_ERROR_CANCEL;
                                break;
                            }
                        }
                        else
                        {
                            stallCounter = 0;
                        }
                    }
                    osThreadYield();
                } while (retVal == MCU_ERROR_NONE);
            }
            else
            {
                // Update the position immediately (NOTE: tmcl response value to tmclCmdMoveToPosition is 'don't care')
                tmclSetPosition(act, (int32_t)tmclResponse.data);
            }
            mcu_actuatorSetBusyStatus(act, 0);
        }
    }
    return retVal;
}
